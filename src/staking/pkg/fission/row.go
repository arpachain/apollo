package fission

type Row struct {
	Name          string `json:"name"`
	HolderAddress string `json:"holderAddress"`
	LevelOne      int64  `json:"levelOne"`
	LevelTwo      int64  `json:"levelTwo"`
	LevelThree    int64  `json:"levelThree"`
	TotalAward    int64  `json:"totalAward"`
}
