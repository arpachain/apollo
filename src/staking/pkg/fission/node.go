package fission

type Node struct {
	ParentHolderAddress string
	Name                string
	Address             string
	PrivateKey          []byte
	PublicKey           string
	HolderAddress       string // ID
	TransactionHash     string
	Activated           bool
	StakingAmount       string
}
