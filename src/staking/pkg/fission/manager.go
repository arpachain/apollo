package fission

import (
	"context"
	"fmt"
	"math/big"

	"github.com/arpachain/apollo/src/common/pkg/config"
	"github.com/arpachain/apollo/src/wallet/pkg/eth"
	"github.com/labstack/gommon/log"
)

type Manager interface {
	SetupNewNode(
		name string,
		holderAddress string,
		parentHolderAddress string,
	) (string, error)
	ActivateNode(
		holderAddress string,
		amount *big.Int,
		transactionHash string,
	) error
	GetChildNodes(holderAddress string, level int) ([]*Node, error)
	GetParentNode(holderAddress string) (*Node, error)
	GetNode(holderAddress string) (*Node, error)
	GetRankings(limit int) ([]*Row, error)
	GetNodeInfo(holderAddress string) (*Row, error)
	GetUnconfirmedNodes() ([]*Node, error)
	BackfillConfirmTransaction(
		stakeholderAddress string,
		amount string,
		transactionHash string,
	) error
	BackfillNode(holderAddress string) error
}

type manager struct {
	database      Database
	client        eth.Client
	defaultConfig config.DefaultConfig
	passphrase    string
}

func NewManager(database Database, client eth.Client, defaultConfig config.DefaultConfig, passphrase string) Manager {
	return &manager{
		database:      database,
		client:        client,
		defaultConfig: defaultConfig,
		passphrase:    passphrase,
	}
}

func (m *manager) SetupNewNode(
	name string,
	holderAddress string,
	parentHolderAddress string,
) (string, error) {
	if parentHolderAddress != "" {
		parentNode, err := m.database.ReadNode(parentHolderAddress)
		if err != nil {
			return "", err
		}
		if !parentNode.Activated {
			return "", fmt.Errorf("inactivated-parent-node")
		}
	} else {
		parentHolderAddress = "root"
	}
	account, err := eth.GenerateAccount(m.passphrase)
	if err != nil {
		return "", err
	}
	newNode := &Node{
		ParentHolderAddress: parentHolderAddress,
		Name:                name,
		PrivateKey:          account.PrivateKey,
		PublicKey:           account.PublicKey,
		Address:             account.Address,
		HolderAddress:       holderAddress,
		Activated:           false,
		TransactionHash:     "",
		StakingAmount:       "",
	}
	_, err = m.database.CreateNode(newNode)
	if err != nil {
		return "", err
	}
	log.Infof("setup new fission node with address: %v", newNode.Address)
	return newNode.Address, nil
}

func (m *manager) ActivateNode(
	holderAddress string,
	amount *big.Int,
	transactionHash string,
) error {
	if transactionHash == "" {
		return fmt.Errorf("must provide valid transaction hash, got: %v", transactionHash)
	}
	ctx := context.Background()
	threshold, err := m.defaultConfig.GetFissionNodeMinStakingAmt()
	if err != nil {
		return err
	}
	if amount.Cmp(threshold) < 0 {
		return fmt.Errorf("not enough arpa")
	}
	err = m.database.SetTransactionHash(holderAddress, transactionHash, amount.String())
	if err != nil {
		return err
	}
	transferConfirmed, err := m.client.ConfirmTransfer(ctx, holderAddress, amount, transactionHash)
	if err != nil {
		return err
	}
	if !transferConfirmed {
		return fmt.Errorf("setting up fission node failed, transfer is not confirmed")
	}
	return m.database.ActivateNode(holderAddress)
}

func (m *manager) GetChildNodes(holderAddress string, level int) ([]*Node, error) {
	return m.database.ReadChildNodes(holderAddress, level)
}

func (m *manager) GetParentNode(holderAddress string) (*Node, error) {
	return nil, nil
}

func (m *manager) GetNode(holderAddress string) (*Node, error) {
	return m.database.ReadNode(holderAddress)
}

func (m *manager) GetRankings(limit int) ([]*Row, error) {
	return m.database.ReadRankingTable(limit)
}

func (m *manager) GetNodeInfo(holderAddress string) (*Row, error) {
	return m.database.ReadInfo(holderAddress)
}

func (m *manager) GetUnconfirmedNodes() ([]*Node, error) {
	return m.database.ReadInactiveNodes()
}

func (m *manager) BackfillConfirmTransaction(
	stakeholderAddress string,
	amount string,
	transactionHash string,
) error {
	return m.client.BackfillConfirmTransaction(stakeholderAddress, amount, transactionHash)
}

func (m *manager) BackfillNode(holderAddress string) error {
	return m.database.ActivateNode(holderAddress)
}
