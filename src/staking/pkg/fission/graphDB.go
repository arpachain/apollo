package fission

import (
	"fmt"
	"strconv"
	"strings"

	"github.com/labstack/gommon/log"

	"github.com/neo4j/neo4j-go-driver/neo4j"
)

type graphDB struct {
	driver neo4j.Driver
}

func NewDatabase(driver neo4j.Driver) Database {
	return &graphDB{
		driver: driver,
	}
}

func (db *graphDB) mapNode(node *Node) map[string]interface{} {
	return map[string]interface{}{
		"ParentHolderAddress": node.ParentHolderAddress,
		"Name":                node.Name,
		"Address":             node.Address,
		"PrivateKey":          node.PrivateKey,
		"PublicKey":           node.PublicKey,
		"HolderAddress":       node.HolderAddress,
		"TransactionHash":     node.TransactionHash,
		"Activated":           node.Activated,
		"StakingAmount":       node.StakingAmount,
	}
}

func (db *graphDB) mapRecord(record neo4j.Record) *Node {
	name, _ := record.Get("Name")
	address, _ := record.Get("Address")
	publicKey, _ := record.Get("PublicKey")
	privateKey, _ := record.Get("PrivateKey")
	holderAddress, _ := record.Get("HolderAddress")
	transactionHash, _ := record.Get("TransactionHash")
	activated, _ := record.Get("Activated")
	stakingAmount, _ := record.Get("StakingAmount")
	return &Node{
		Name:            name.(string),
		Address:         address.(string),
		PrivateKey:      privateKey.([]byte),
		PublicKey:       publicKey.(string),
		HolderAddress:   holderAddress.(string),
		TransactionHash: transactionHash.(string),
		Activated:       activated.(bool),
		StakingAmount:   stakingAmount.(string),
	}
}

func (db *graphDB) getParams() string {
	return "HolderAddress: $HolderAddress, " +
		"Name: $Name, " +
		"Address: $Address, " +
		"PrivateKey: $PrivateKey, " +
		"PublicKey: $PublicKey, " +
		"TransactionHash: $TransactionHash, " +
		"Activated: $Activated, " +
		"StakingAmount: $StakingAmount "
}

func (db *graphDB) CreateNode(node *Node) (string, error) {
	session, err := db.driver.Session(neo4j.AccessModeWrite)
	if err != nil {
		return "", err
	}
	defer session.Close()
	node.ParentHolderAddress = strings.ToLower(node.ParentHolderAddress)
	node.Address = strings.ToLower(node.Address)
	node.HolderAddress = strings.ToLower(node.HolderAddress)
	node.TransactionHash = strings.ToLower(node.TransactionHash)
	nodeContent := db.mapNode(node)
	params := db.getParams()
	cypher := fmt.Sprintf(
		"MATCH (parent:FissionNode { HolderAddress: $ParentHolderAddress }) "+
			"CREATE (parent)-[refer:REFER]->(child:FissionNode{ %v }), (child)-[referredBy:REFERRED_BY]->(parent) "+
			"RETURN child.HolderAddress",
		params,
	)
	holderAddress, err := session.WriteTransaction(
		func(transaction neo4j.Transaction) (interface{}, error) {
			result, err := transaction.Run(
				cypher,
				nodeContent,
			)
			if err != nil {
				return nil, err
			}
			if result.Next() {
				return result.Record().GetByIndex(0), nil
			}
			return nil, result.Err()
		})
	if err != nil {
		return "", db.handleDbError(err)
	}
	if holderAddress == nil {
		return "", fmt.Errorf("parent node not found for address: %v", node.ParentHolderAddress)
	}
	return holderAddress.(string), nil
}

func (db *graphDB) ReadNode(holderAddress string) (*Node, error) {
	session, err := db.driver.Session(neo4j.AccessModeWrite)
	if err != nil {
		return nil, err
	}
	defer session.Close()
	cypher :=
		"MATCH (node:FissionNode { HolderAddress: $HolderAddress }) " +
			"RETURN " + db.getReturnFields()
	result, err := session.Run(cypher, map[string]interface{}{"HolderAddress": strings.ToLower(holderAddress)})
	if err != nil {
		return nil, err
	}
	var node *Node
	for result.Next() {
		log.Infof("Found node with holder address: %v\n", holderAddress)
		node = db.mapRecord(result.Record())
	}
	if result.Err() != nil {
		return nil, result.Err()
	}
	if node == nil {
		return nil, fmt.Errorf("no node found with holder address: %v", holderAddress)
	}
	return node, nil
}

func (db *graphDB) ReadInactiveNodes() ([]*Node, error) {
	session, err := db.driver.Session(neo4j.AccessModeWrite)
	if err != nil {
		return nil, err
	}
	defer session.Close()
	cypher :=
		"MATCH (node:FissionNode { Activated: $Activated }) " +
			"WHERE node.TransactionHash <> '' AND node.StakingAmount <> '' AND node.TransactionHash STARTS WITH '0x' " +
			"RETURN " + db.getReturnFields()
	result, err := session.Run(cypher, map[string]interface{}{"Activated": false})
	if err != nil {
		return nil, err
	}
	nodes := make([]*Node, 0)
	var node *Node
	for result.Next() {
		node = db.mapRecord(result.Record())
		log.Infof("Found inactive node with holder address: %v\n", node.HolderAddress)
		nodes = append(nodes, node)
	}
	if result.Err() != nil {
		return nil, result.Err()
	}
	if node == nil || len(nodes) == 0 {
		return nil, fmt.Errorf("no inactive node was found")
	}
	return nodes, nil
}

func (db *graphDB) ReadInfo(holderAddress string) (*Row, error) {
	session, err := db.driver.Session(neo4j.AccessModeWrite)
	if err != nil {
		return nil, err
	}
	defer session.Close()
	cypher :=
		"MATCH(n:FissionNode)-[:REFER*]->(m:FissionNode) " +
			"WHERE n.HolderAddress <> $Root AND n.HolderAddress = $HolderAddress AND n.Activated = true AND m.Activated = true " +
			"RETURN n.Name AS Name, " +
			"n.HolderAddress AS HolderAddress, " +
			"SIZE((n)-[:REFER*1]->()) AS LevelOne, " +
			"SIZE((n)-[:REFER*2]->()) AS LevelTwo, " +
			"SIZE((n)-[:REFER*3]->()) AS LevelThree, " +
			"SIZE((n)-[:REFER*1]->()) * 100 + SIZE((n)-[:REFER*2]->()) * 30 + SIZE((n)-[:REFER*3]->()) * 10 AS TotalReward "
	result, err := session.Run(cypher, map[string]interface{}{
		"HolderAddress": strings.ToLower(holderAddress),
		"Root":          "root",
	})
	if err != nil {
		return nil, err
	}
	var row *Row
	for result.Next() {
		log.Infof("Found node info with holder address: %v\n", holderAddress)
		row = db.mapRecordToRow(result.Record())
	}
	if result.Err() != nil {
		return nil, result.Err()
	}
	if row == nil {
		return &Row{}, nil
	}
	return row, nil
}

func (db *graphDB) SetTransactionHash(holderAddress, transactionHash, stakingAmount string) error {
	session, err := db.driver.Session(neo4j.AccessModeWrite)
	if err != nil {
		return err
	}
	defer session.Close()
	cypher :=
		"MATCH (n:FissionNode { HolderAddress: $HolderAddress }) " +
			"SET " +
			"n.TransactionHash = $TransactionHash, " +
			"n.StakingAmount = $StakingAmount " +
			"RETURN n.Activated "

	_, err = session.WriteTransaction(
		func(transaction neo4j.Transaction) (interface{}, error) {
			result, err := transaction.Run(
				cypher,
				map[string]interface{}{
					"HolderAddress":   strings.ToLower(holderAddress),
					"TransactionHash": strings.ToLower(transactionHash),
					"StakingAmount":   stakingAmount,
				},
			)
			if err != nil {
				return nil, err
			}
			if result.Next() {
				return result.Record().GetByIndex(0), nil
			}
			return nil, result.Err()
		})
	return err
}

func (db *graphDB) ActivateNode(holderAddress string) error {
	session, err := db.driver.Session(neo4j.AccessModeWrite)
	if err != nil {
		return err
	}
	defer session.Close()
	cypher :=
		"MATCH (n:FissionNode { HolderAddress: $HolderAddress }) " +
			"SET n.Activated = true " +
			"RETURN n.Activated "

	_, err = session.WriteTransaction(
		func(transaction neo4j.Transaction) (interface{}, error) {
			result, err := transaction.Run(
				cypher,
				map[string]interface{}{
					"HolderAddress": strings.ToLower(holderAddress),
				},
			)
			if err != nil {
				return nil, err
			}
			if result.Next() {
				return result.Record().GetByIndex(0), nil
			}
			return nil, result.Err()
		})
	return err
}

func (db *graphDB) ReadChildNodes(holderAddress string, level int) ([]*Node, error) {
	session, err := db.driver.Session(neo4j.AccessModeWrite)
	if err != nil {
		return nil, err
	}
	defer session.Close()
	levelString := ""
	if level > 0 {
		levelString = strconv.Itoa(level)
	}
	cypher :=
		"MATCH (:FissionNode { HolderAddress: $HolderAddress })-[:REFER*" + levelString + "]->(node) " +
			"RETURN " + db.getReturnFields()
	result, err := session.Run(cypher, map[string]interface{}{"HolderAddress": strings.ToLower(holderAddress)})
	if err != nil {
		return nil, err
	}
	nodes := make([]*Node, 0)
	for result.Next() {
		nodes = append(nodes, db.mapRecord(result.Record()))
	}
	return nodes, nil
}

func (db *graphDB) ReadRankingTable(limit int) ([]*Row, error) {
	session, err := db.driver.Session(neo4j.AccessModeWrite)
	if err != nil {
		return nil, err
	}
	defer session.Close()
	cypher :=
		"MATCH(n:FissionNode) " +
			"WHERE n.HolderAddress <> $Root AND n.Activated = true " +
			"RETURN n.Name AS Name, " +
			"n.HolderAddress AS HolderAddress, " +
			"SIZE((n)-[:REFER*1]->(:FissionNode{Activated: true})) AS LevelOne, " +
			"SIZE((n)-[:REFER*2]->(:FissionNode{Activated: true})) AS LevelTwo, " +
			"SIZE((n)-[:REFER*3]->(:FissionNode{Activated: true})) AS LevelThree, " +
			"SIZE((n)-[:REFER*1]->(:FissionNode{Activated: true})) * 100 + SIZE((n)-[:REFER*2]->(:FissionNode{Activated: true})) * 30 + SIZE((n)-[:REFER*3]->(:FissionNode{Activated: true})) * 10 AS TotalReward " +
			"ORDER BY TotalReward DESC LIMIT $Limit"
	result, err := session.Run(cypher, map[string]interface{}{"Root": "root", "Limit": limit})
	if err != nil {
		return nil, err
	}
	rows := make([]*Row, 0)
	for result.Next() {
		record := result.Record()
		row := db.mapRecordToRow(record)
		rows = append(rows, row)
	}
	if result.Err() != nil {
		return nil, result.Err()
	}
	return rows, nil
}

func (db *graphDB) getReturnFields() string {
	return "node.HolderAddress as HolderAddress, " +
		"node.Name as Name, " +
		"node.Address as Address, " +
		"node.PrivateKey as PrivateKey, " +
		"node.PublicKey as PublicKey, " +
		"node.TransactionHash as TransactionHash, " +
		"node.Activated as Activated, " +
		"node.StakingAmount as StakingAmount "
}

func (db *graphDB) handleDbError(err error) error {
	message := err.Error()
	log.Infof(message)
	if strings.Contains(message, "already exists") {
		return fmt.Errorf("node-already-exist")
	}
	return err
}

func (db *graphDB) mapRecordToRow(record neo4j.Record) *Row {
	name, _ := record.Get("Name")
	holderAddress, _ := record.Get("HolderAddress")
	levelOne, _ := record.Get("LevelOne")
	levelTwo, _ := record.Get("LevelTwo")
	levelThree, _ := record.Get("LevelThree")
	totalReward, _ := record.Get("TotalReward")
	return &Row{
		Name:          name.(string),
		HolderAddress: holderAddress.(string),
		LevelOne:      levelOne.(int64),
		LevelTwo:      levelTwo.(int64),
		LevelThree:    levelThree.(int64),
		TotalAward:    totalReward.(int64),
	}
}
