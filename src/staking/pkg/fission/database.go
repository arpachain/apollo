package fission

type Database interface {
	CreateNode(node *Node) (string, error)
	ReadNode(holderAddress string) (*Node, error)
	SetTransactionHash(holderAddress, transactionHash, stakingAmount string) error
	ReadInactiveNodes() ([]*Node, error)
	ActivateNode(holderAddress string) error
	ReadChildNodes(holderAddress string, level int) ([]*Node, error)
	ReadRankingTable(limit int) ([]*Row, error)
	ReadInfo(holderAddress string) (*Row, error)
}
