package stakeholder

import (
	"strings"

	"github.com/labstack/gommon/log"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

type mongo struct {
	DB *mgo.Session
}

func NewMongo(db *mgo.Session) Database {
	return &mongo{
		DB: db,
	}
}

func (m *mongo) CreateNode(node *Node) error {
	db := m.DB.Clone()
	defer db.Close()
	node.Address = strings.ToLower(node.Address)
	node.StakeholderAddress = strings.ToLower(node.StakeholderAddress)
	if err := db.DB("apollo").C("nodes").Insert(node); err != nil {
		return err
	}
	return nil
}

func (m *mongo) ReadAllNodes() ([]*Node, error) {
	db := m.DB.Clone()
	defer db.Close()
	nodes := []*Node{}
	if err := db.DB("apollo").C("nodes").
		Find(nil).
		All(&nodes); err != nil {
		return nil, err
	}
	return nodes, nil
}

func (m *mongo) ReadNode(id bson.ObjectId) (*Node, error) {
	n := new(Node)
	db := m.DB.Clone()
	defer db.Close()
	if err := db.DB("apollo").C("nodes").
		FindId(id).One(n); err != nil {
		return nil, err
	}
	return n, nil
}

func (m *mongo) UpdateNode(newNode *Node) error {
	db := m.DB.Clone()
	defer db.Close()
	if err := db.DB("apollo").C("nodes").
		Update(
			bson.M{"_id": newNode.ID},
			bson.M{
				"$set": bson.M{
					"name":      newNode.Name,
					"activated": newNode.Activated,
					"votable":   newNode.Votable,
				},
			},
		); err != nil {
		return err
	}
	return nil
}

func (m *mongo) ReadNodes(holderAddress string) ([]*Node, error) {
	db := m.DB.Clone()
	defer db.Close()
	holderAddress = strings.ToLower(holderAddress)
	nodes := []*Node{}
	if err := db.DB("apollo").C("nodes").
		Find(bson.M{"stakeholderAddress": holderAddress}).
		All(&nodes); err != nil {
		return nil, err
	}
	return nodes, nil
}

func (m *mongo) CheckSuperNode(holderAddress string) bool {
	db := m.DB.Clone()
	defer db.Close()
	n := new(ReservedNode)
	if err := db.DB("apollo").C("whitelist").
		Find(bson.M{"stakeholderAddress": strings.ToLower(holderAddress)}).One(n); err != nil {
		log.Infof("checking whitelist error: %v", err.Error())
		return false
	}
	log.Infof("checking whitelist: %v", n)
	if n != nil && n.StakeholderAddress != "" {
		return true
	}
	return false
}
