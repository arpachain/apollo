package stakeholder

import (
	"errors"
	"fmt"
	"sync"

	"github.com/labstack/gommon/log"

	"github.com/globalsign/mgo/bson"
)

type memory struct {
	mu      *sync.Mutex
	nodeMap map[bson.ObjectId]Node /*key: Address*/
}

func NewMemory() Database {
	return &memory{
		mu:      &sync.Mutex{},
		nodeMap: map[bson.ObjectId]Node{},
	}
}

func (m *memory) CreateNode(node *Node) error {
	m.mu.Lock()
	defer m.mu.Unlock()
	if _, exists := m.nodeMap[node.ID]; exists {
		return errors.New("node with same id address already exists")
	}
	m.nodeMap[node.ID] = Node{
		ID:                 node.ID,
		Name:               node.Name,
		Address:            node.Address,
		PrivateKey:         node.PrivateKey,
		PublicKey:          node.PublicKey,
		StakeholderAddress: node.StakeholderAddress,
		Type:               node.Type,
		CreatedAt:          node.CreatedAt,
		Activated:          node.Activated,
		Votable:            node.Votable,
	}
	log.Infof("node is created\n")
	log.Infof("id:%v\n", node.ID)
	log.Infof("address:%v\n", node.Address)
	log.Infof("stakeholder address:%v\n", node.StakeholderAddress)
	log.Infof("type: %v\n", node.Type)
	return nil
}

func (m *memory) ReadAllNodes() ([]*Node, error) {
	m.mu.Lock()
	defer m.mu.Unlock()
	nodes := make([]*Node, 0)
	for _, node := range m.nodeMap {
		nodes = append(nodes, &node)
	}
	return nodes, nil
}

func (m *memory) ReadNode(id bson.ObjectId) (*Node, error) {
	m.mu.Lock()
	defer m.mu.Unlock()
	originalNode, exists := m.nodeMap[id]
	if !exists {
		return nil, fmt.Errorf("node with the id[%v] does not exists", id)
	}
	node := &Node{
		ID:                 originalNode.ID,
		Name:               originalNode.Name,
		Address:            originalNode.Address,
		PrivateKey:         originalNode.PrivateKey,
		PublicKey:          originalNode.PublicKey,
		StakeholderAddress: originalNode.StakeholderAddress,
		Type:               originalNode.Type,
		CreatedAt:          originalNode.CreatedAt,
		Activated:          originalNode.Activated,
		Votable:            originalNode.Votable,
	}
	return node, nil
}

func (m *memory) UpdateNode(newNode *Node) error {
	m.mu.Lock()
	defer m.mu.Unlock()
	originalNode, exists := m.nodeMap[newNode.ID]
	if !exists {
		return fmt.Errorf("node with the id[%v] does not exists", newNode.ID)
	}
	m.nodeMap[newNode.ID] = Node{
		ID:                 originalNode.ID,
		Name:               newNode.Name,
		Address:            newNode.Address,
		PrivateKey:         newNode.PrivateKey,
		PublicKey:          newNode.PublicKey,
		StakeholderAddress: newNode.StakeholderAddress,
		Type:               newNode.Type,
		CreatedAt:          newNode.CreatedAt,
		Activated:          newNode.Activated,
		Votable:            newNode.Votable,
	}
	return nil
}

func (m *memory) ReadNodes(holderAddress string) ([]*Node, error) {
	m.mu.Lock()
	defer m.mu.Unlock()
	nodes := make([]*Node, 0)
	for _, originalNode := range m.nodeMap {
		if originalNode.StakeholderAddress == holderAddress {
			node := &Node{
				ID:                 originalNode.ID,
				Name:               originalNode.Name,
				Address:            originalNode.Address,
				PrivateKey:         originalNode.PrivateKey,
				PublicKey:          originalNode.PublicKey,
				StakeholderAddress: originalNode.StakeholderAddress,
				Type:               originalNode.Type,
				CreatedAt:          originalNode.CreatedAt,
				Activated:          originalNode.Activated,
				Votable:            originalNode.Votable,
			}
			nodes = append(nodes, node)
		}
	}
	return nodes, nil
}

func (m *memory) CheckSuperNode(holderAddress string) bool { return true }
