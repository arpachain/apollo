package stakeholder

import (
	"fmt"
	"time"

	"github.com/labstack/gommon/log"

	"github.com/arpachain/apollo/src/common/pkg/config"
	"github.com/arpachain/apollo/src/wallet/pkg/eth"
	"github.com/globalsign/mgo/bson"
)

type Manager interface {
	SetupNewNode(name, address string, nodeType config.NodeType) (string, *bson.ObjectId, error)
	GetNode(id bson.ObjectId) (*Node, error)
	GetAllNodes() ([]*Node, error)
	ActivateNode(id bson.ObjectId) error
	AllowVoteForNode(id bson.ObjectId) error
	GetNodes(holderAddress string) ([]*Node, error)
}

type manager struct {
	passphrase string
	database   Database
}

func NewManager(passphrase string, database Database) Manager {
	return &manager{
		passphrase: passphrase,
		database:   database,
	}
}

func (m *manager) SetupNewNode(name, address string, nodeType config.NodeType) (string, *bson.ObjectId, error) {
	if nodeType == config.SuperNode && !m.database.CheckSuperNode(address) {
		return "", nil, fmt.Errorf("not-reserved")
	}
	account, err := eth.GenerateAccount(m.passphrase)
	if err != nil {
		return "", nil, err
	}
	newNode := &Node{
		ID:                 bson.NewObjectId(),
		Name:               name,
		Type:               nodeType,
		Address:            account.Address,
		PrivateKey:         account.PrivateKey,
		PublicKey:          account.PublicKey,
		StakeholderAddress: address,
		CreatedAt:          time.Now(),
		Activated:          false,
		Votable:            false,
	}
	log.Infof("setup new node with address: %v", newNode.Address)
	err = m.database.CreateNode(newNode)
	if err != nil {
		log.Errorf("setup new node failed for reason: %v", err.Error())
		return "", nil, err
	}
	return newNode.Address, &newNode.ID, nil
}

func (m *manager) GetNode(id bson.ObjectId) (*Node, error) {
	return m.database.ReadNode(id)
}

func (m *manager) GetAllNodes() ([]*Node, error) {
	return m.database.ReadAllNodes()
}

func (m *manager) ActivateNode(id bson.ObjectId) error {
	node, err := m.database.ReadNode(id)
	if err != nil {
		return err
	}
	node.Activated = true
	err = m.database.UpdateNode(node)
	if err != nil {
		return err
	}
	return nil
}

func (m *manager) AllowVoteForNode(id bson.ObjectId) error {
	node, err := m.database.ReadNode(id)
	if err != nil {
		return err
	}
	node.Votable = true
	err = m.database.UpdateNode(node)
	if err != nil {
		return err
	}
	return nil
}

func (m *manager) GetNodes(holderAddress string) ([]*Node, error) {
	return m.database.ReadNodes(holderAddress)
}
