package stakeholder

import "github.com/globalsign/mgo/bson"

type Database interface {
	CreateNode(node *Node) error
	ReadAllNodes() ([]*Node, error)
	ReadNode(id bson.ObjectId) (*Node, error)
	ReadNodes(holderAddress string) ([]*Node, error)
	UpdateNode(newNode *Node) error
	CheckSuperNode(holderAddress string) bool
}
