package stakeholder

import (
	"time"

	"github.com/arpachain/apollo/src/common/pkg/config"
	"github.com/globalsign/mgo/bson"
)

type Node struct {
	ID                 bson.ObjectId   `json:"id" bson:"_id,omitempty"`
	Name               string          `json:"name" bson:"name"`
	Type               config.NodeType `json:"nodeType" bson:"nodeType"`
	Address            string          `json:"address" bson:"address"`
	PrivateKey         []byte          `json:"-" bson:"privateKey"`
	PublicKey          string          `json:"publicKey" bson:"publicKey"`
	StakeholderAddress string          `json:"stakeholderAddress" bson:"stakeholderAddress"`
	CreatedAt          time.Time       `json:"createdAt" bson:"createdAt"`
	Activated          bool            `json:"activated" bson:"activated"`
	Votable            bool            `json:"votable" bson:"votable"`
}
