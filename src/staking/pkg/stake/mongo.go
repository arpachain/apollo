package stake

import (
	"fmt"
	"strings"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/labstack/gommon/log"
)

type mongo struct {
	DB *mgo.Session
}

func NewMongo(db *mgo.Session) Database {
	return &mongo{
		DB: db,
	}
}

func (m *mongo) CreateStake(stake *Stake) error {
	db := m.DB.Clone()
	defer db.Close()
	stake.StakeholderAddress = strings.ToLower(stake.StakeholderAddress)
	stake.TransactionHash = strings.ToLower(stake.TransactionHash)
	if err := db.DB("apollo").C("stakes").Insert(stake); err != nil {
		return err
	}
	return nil
}

func (m *mongo) CreateAdmission(admission *Admission) error {
	db := m.DB.Clone()
	defer db.Close()
	admission.StakeholderAddress = strings.ToLower(admission.StakeholderAddress)
	admission.TransactionHash = strings.ToLower(admission.TransactionHash)
	if err := db.DB("apollo").C("admissions").Insert(admission); err != nil {
		return err
	}
	return nil
}

func (m *mongo) ReadAllStakes(nodeID bson.ObjectId) ([]*Stake, error) {
	db := m.DB.Clone()
	defer db.Close()
	stakes := []*Stake{}
	if err := db.DB("apollo").C("stakes").
		Find(bson.M{"nodeId": nodeID, "confirmed": true}).
		All(&stakes); err != nil {
		return nil, err
	}
	return stakes, nil
}

func (m *mongo) ReadStakes(nodeID bson.ObjectId, stakeholderAddress string) ([]*Stake, error) {
	db := m.DB.Clone()
	defer db.Close()
	stakeholderAddress = strings.ToLower(stakeholderAddress)
	stakes := []*Stake{}
	if err := db.DB("apollo").C("stakes").
		Find(bson.M{"nodeId": nodeID, "confirmed": true, "stakeholderAddress": stakeholderAddress}).
		All(&stakes); err != nil {
		return nil, err
	}
	return stakes, nil
}

func (m *mongo) ReadStakingOnlyNodeIDs(stakeholderAddress string, excludedNodeIDs []string) ([]string, error) {
	db := m.DB.Clone()
	defer db.Close()
	stakes := []*Stake{}
	stakeholderAddress = strings.ToLower(stakeholderAddress)
	if err := db.DB("apollo").C("stakes").
		Find(bson.M{"confirmed": true, "stakeholderAddress": stakeholderAddress}).
		All(&stakes); err != nil {
		return nil, err
	}
	nodeIDs := make([]string, 0)
	nodeIDMap := map[string]bool{}
	excludedNodeIDMap := map[string]bool{}
	for _, excludedNodeID := range excludedNodeIDs {
		if _, exists := excludedNodeIDMap[excludedNodeID]; exists {
			return nil, fmt.Errorf("excluded node ids must be unique")
		}
		excludedNodeIDMap[excludedNodeID] = true
	}
	for _, stake := range stakes {
		if _, exists := excludedNodeIDMap[stake.NodeID.Hex()]; exists {
			// Excluded
			log.Infof("excluding node id: %v", stake.NodeID.Hex())
		} else {
			if _, exists := nodeIDMap[stake.NodeID.Hex()]; exists {
				log.Infof("duplicated node id: %v", stake.NodeID.Hex())
				continue
			} else {
				log.Infof("including node id: %v", stake.NodeID.Hex())
				nodeIDMap[stake.NodeID.Hex()] = true
				nodeIDs = append(nodeIDs, stake.NodeID.Hex())
			}
		}
	}
	return nodeIDs, nil
}

func (m *mongo) ConfirmStake(transactionHash string) error {
	db := m.DB.Clone()
	defer db.Close()
	if err := db.DB("apollo").C("stakes").
		Update(
			bson.M{"transactionHash": strings.ToLower(transactionHash)},
			bson.M{
				"$set": bson.M{
					"confirmed": true,
				},
			},
		); err != nil {
		return err
	}
	return nil
}

func (m *mongo) ConfirmAdmission(nodeID bson.ObjectId) error {
	db := m.DB.Clone()
	defer db.Close()
	if err := db.DB("apollo").C("admissions").
		Update(
			bson.M{"nodeId": nodeID},
			bson.M{
				"$set": bson.M{
					"confirmed": true,
				},
			},
		); err != nil {
		return err
	}
	return nil
}

func (m *mongo) ReadUnconfirmedAdmissions() ([]*Admission, error) {
	db := m.DB.Clone()
	defer db.Close()
	admissions := []*Admission{}
	if err := db.DB("apollo").C("admissions").
		Find(bson.M{"confirmed": false}).
		All(&admissions); err != nil {
		return nil, err
	}
	return admissions, nil
}

func (m *mongo) ReadUnconfirmedStakes() ([]*Stake, error) {
	db := m.DB.Clone()
	defer db.Close()
	stakes := []*Stake{}
	if err := db.DB("apollo").C("stakes").
		Find(bson.M{"confirmed": false}).
		All(&stakes); err != nil {
		return nil, err
	}
	return stakes, nil
}
