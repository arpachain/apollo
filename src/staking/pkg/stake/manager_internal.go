package stake

import (
	"time"

	"github.com/globalsign/mgo/bson"
)

func (m *manager) generateStake(
	address string,
	amount string,
	transactionHash string,
	nodeID bson.ObjectId,
	isInitialStake bool,
) *Stake {
	return &Stake{
		NodeID:             nodeID,
		StakeholderAddress: address,
		Amount:             amount,
		TransactionHash:    transactionHash,
		TimeStamp:          time.Now(),
		Confirmed:          false,
		IsInitialStake:     isInitialStake,
	}
}

func (m *manager) generateAdmission(address string, amount string, transactionHash string, nodeID bson.ObjectId) *Admission {
	return &Admission{
		NodeID:             nodeID,
		StakeholderAddress: address,
		Amount:             amount,
		TransactionHash:    transactionHash,
		TimeStamp:          time.Now(),
		Confirmed:          false,
	}
}
