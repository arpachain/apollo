package stake

import (
	"context"
	"fmt"
	"math/big"

	"github.com/labstack/gommon/log"

	"github.com/arpachain/apollo/src/common/pkg/config"
	"github.com/arpachain/apollo/src/wallet/pkg/eth"
	"github.com/globalsign/mgo/bson"
)

type Manager interface {
	Apply(
		admissionFeeAmount *big.Int,
		address string,
		transactionHash string,
		nodeType config.NodeType,
		nodeID bson.ObjectId,
	) error
	Stake(
		amount *big.Int,
		address string,
		transactionHash string,
		isInitialStake bool,
		nodeType config.NodeType,
		nodeID bson.ObjectId,
	) error
	Unstake(amount *big.Int, address string) (string, error)
	GetTotalStakingAmount(nodeID bson.ObjectId) (*big.Int, error)
	GetStakingAmount(nodeID bson.ObjectId, stakeholderAddress string) (*big.Int, error)
	GetStakingOnlyNodeIDs(stakeholderAddress string, excludedNodeIDs []string) ([]string, error)
	GetUnconfirmedAdmissions() ([]*Admission, error)
	GetUnconfirmedStakes() ([]*Stake, error)
	ConfirmAdmission(nodeID bson.ObjectId) error
	ConfirmStake(transactionHash string) error
	BackfillConfirmTransaction(
		stakeholderAddress string,
		amount string,
		transactionHash string,
	) error
}

type manager struct {
	database      Database
	client        eth.Client
	defaultConfig config.DefaultConfig
}

func NewManager(
	database Database,
	client eth.Client,
	defaultConfig config.DefaultConfig) Manager {
	return &manager{
		database:      database,
		client:        client,
		defaultConfig: defaultConfig,
	}
}

func (m *manager) Apply(
	admissionFeeAmount *big.Int,
	address string,
	transactionHash string,
	nodeType config.NodeType,
	nodeID bson.ObjectId,
) error {
	ctx := context.Background()
	nodeConfig := m.defaultConfig.GetNodeConfigByType(nodeType)
	threshold, err := m.defaultConfig.GetAdmissionFee(nodeConfig)
	if err != nil {
		return err
	}
	if admissionFeeAmount.Cmp(threshold) < 0 {
		return fmt.Errorf("not enough arpa")
	}
	admissionFeeAmountString := nodeConfig.AdmissionFee + config.EighteenZeros
	admission := m.generateAdmission(address, admissionFeeAmountString, transactionHash, nodeID)
	err = m.database.CreateAdmission(admission)
	if err != nil {
		log.Errorf("atomic operation failed: creating admission to db for reason: %v", err)
	}
	transferConfirmed, err := m.client.ConfirmTransfer(ctx, address, admissionFeeAmount, transactionHash)
	if err != nil {
		return err
	}
	if !transferConfirmed {
		return fmt.Errorf("admission failed, transfer is not confirmed")
	}
	err = m.database.ConfirmAdmission(nodeID)
	if err != nil {
		return err
	}
	return nil
}

func (m *manager) Stake(
	amount *big.Int,
	address string,
	transactionHash string,
	isInitialStake bool,
	nodeType config.NodeType,
	nodeID bson.ObjectId,
) error {
	if transactionHash == "" {
		return fmt.Errorf("must provide valid transaction hash, got: %v", transactionHash)
	}
	ctx := context.Background()
	var threshold *big.Int
	var err error
	if isInitialStake {
		nodeConfig := m.defaultConfig.GetNodeConfigByType(nodeType)
		threshold, err = m.defaultConfig.GetMinimumStakingAmount(nodeConfig)
		if err != nil {
			return err
		}
	} else {
		threshold, err = m.defaultConfig.GetMinStakingAmount()
		if err != nil {
			return err
		}
	}
	if amount.Cmp(threshold) < 0 {
		return fmt.Errorf("not enough arpa")
	}
	stake := m.generateStake(address, amount.String(), transactionHash, nodeID, isInitialStake)
	err = m.database.CreateStake(stake)
	if err != nil {
		return err
	}
	transferConfirmed, err := m.client.ConfirmTransfer(ctx, address, amount, transactionHash)
	if err != nil {
		return err
	}
	if !transferConfirmed {
		return fmt.Errorf("staking failed, transfer is not confirmed")
	}
	err = m.database.ConfirmStake(transactionHash)
	if err != nil {
		return err
	}
	return nil
}

func (m *manager) Unstake(amount *big.Int, address string) (string, error) {
	return "", nil
}

func (m *manager) GetTotalStakingAmount(nodeID bson.ObjectId) (*big.Int, error) {
	stakes, err := m.database.ReadAllStakes(nodeID)
	if err != nil {
		return nil, err
	}
	totalAmount := new(big.Int)
	for _, stake := range stakes {
		amount := new(big.Int)
		amount, _ = amount.SetString(stake.Amount, 10)
		totalAmount = totalAmount.Add(totalAmount, amount)
	}
	return totalAmount, nil
}

func (m *manager) GetStakingAmount(nodeID bson.ObjectId, stakeholderAddress string) (*big.Int, error) {
	stakes, err := m.database.ReadStakes(nodeID, stakeholderAddress)
	if err != nil {
		return nil, err
	}
	totalAmount := new(big.Int)
	for _, stake := range stakes {
		amount := new(big.Int)
		amount, _ = amount.SetString(stake.Amount, 10)
		totalAmount = totalAmount.Add(totalAmount, amount)
	}
	return totalAmount, nil
}

func (m *manager) GetStakingOnlyNodeIDs(stakeholderAddress string, excludedNodeIDs []string) ([]string, error) {
	return m.database.ReadStakingOnlyNodeIDs(stakeholderAddress, excludedNodeIDs)
}

func (m *manager) GetUnconfirmedAdmissions() ([]*Admission, error) {
	return m.database.ReadUnconfirmedAdmissions()
}

func (m *manager) GetUnconfirmedStakes() ([]*Stake, error) {
	return m.database.ReadUnconfirmedStakes()
}

func (m *manager) ConfirmAdmission(nodeID bson.ObjectId) error {
	return m.database.ConfirmAdmission(nodeID)
}

func (m *manager) ConfirmStake(transactionHash string) error {
	return m.database.ConfirmStake(transactionHash)
}

func (m *manager) BackfillConfirmTransaction(
	stakeholderAddress string,
	amount string,
	transactionHash string,
) error {
	return m.client.BackfillConfirmTransaction(stakeholderAddress, amount, transactionHash)
}
