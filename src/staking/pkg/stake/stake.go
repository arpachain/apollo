package stake

import (
	"time"

	"github.com/globalsign/mgo/bson"
)

type Stake struct {
	NodeID             bson.ObjectId `json:"nodeId" bson:"nodeId"`
	StakeholderAddress string        `json:"stakeholderAddress" bson:"stakeholderAddress"`
	Amount             string        `json:"amount" bson:"amount"`
	TransactionHash    string        `json:"transactionHash" bson:"transactionHash"`
	TimeStamp          time.Time     `json:"timeStamp" bson:"timeStamp"`
	Confirmed          bool          `json:"confirmed" bson:"confirmed"`
	IsInitialStake     bool          `json:"isInitialStake" bson:"isInitialStake"`
}
