package stake

import "github.com/globalsign/mgo/bson"

type Database interface {
	CreateStake(stake *Stake) error
	CreateAdmission(admission *Admission) error
	ReadAllStakes(nodeID bson.ObjectId) ([]*Stake, error)
	ReadStakes(nodeID bson.ObjectId, stakeholderAddress string) ([]*Stake, error)
	ReadStakingOnlyNodeIDs(stakeholderAddress string, excludedNodeIDs []string) ([]string, error)
	ConfirmStake(transactionHash string) error
	ConfirmAdmission(nodeID bson.ObjectId) error
	ReadUnconfirmedAdmissions() ([]*Admission, error)
	ReadUnconfirmedStakes() ([]*Stake, error)
}
