package stake

import (
	"errors"
	"sync"

	"github.com/labstack/gommon/log"

	"github.com/globalsign/mgo/bson"
)

type memory struct {
	mu           *sync.Mutex
	stakeMap     map[string]Stake     /*key: StakeholderAddress*/
	admissionMap map[string]Admission /*key: StakeholderAddress*/
}

func NewMemory() Database {
	return &memory{
		mu:           &sync.Mutex{},
		stakeMap:     map[string]Stake{},
		admissionMap: map[string]Admission{},
	}
}

func (m *memory) CreateStake(stake *Stake) error {
	m.mu.Lock()
	defer m.mu.Unlock()
	if _, exists := m.stakeMap[stake.StakeholderAddress]; exists {
		return errors.New("stake with the same address already exists")
	}
	m.stakeMap[stake.StakeholderAddress] = Stake{
		NodeID:             stake.NodeID,
		StakeholderAddress: stake.StakeholderAddress,
		Amount:             stake.Amount,
		TransactionHash:    stake.TransactionHash,
		TimeStamp:          stake.TimeStamp,
	}
	log.Infof("stake is created\n")
	log.Infof("stakeholder address:%v\n", stake.StakeholderAddress)
	log.Infof("amount: %v\n", stake.Amount)
	log.Infof("tx hash: %v\n", stake.TransactionHash)
	return nil
}

func (m *memory) CreateAdmission(admission *Admission) error {
	m.mu.Lock()
	defer m.mu.Unlock()
	if _, exists := m.admissionMap[admission.StakeholderAddress]; exists {
		return errors.New("admission with the same address already exists")
	}
	m.admissionMap[admission.StakeholderAddress] = Admission{
		NodeID:             admission.NodeID,
		StakeholderAddress: admission.StakeholderAddress,
		Amount:             admission.Amount,
		TransactionHash:    admission.TransactionHash,
		TimeStamp:          admission.TimeStamp,
	}
	log.Infof("admission is created\n")
	log.Infof("stakeholder address:%v\n", admission.StakeholderAddress)
	log.Infof("amount: %v\n", admission.Amount)
	log.Infof("tx hash: %v\n", admission.TransactionHash)
	return nil
}

func (m *memory) ReadAllStakes(nodeID bson.ObjectId) ([]*Stake, error) {
	m.mu.Lock()
	defer m.mu.Unlock()
	stakes := make([]*Stake, 0)
	for _, stake := range m.stakeMap {
		if stake.NodeID == nodeID {
			stakes = append(stakes, &stake)
		}
	}
	return stakes, nil
}

func (m *memory) ReadStakes(nodeID bson.ObjectId, stakeholderAddress string) ([]*Stake, error) {
	m.mu.Lock()
	defer m.mu.Unlock()
	stakes := make([]*Stake, 0)
	for _, stake := range m.stakeMap {
		if stake.NodeID == nodeID && stake.StakeholderAddress == stakeholderAddress {
			stakes = append(stakes, &stake)
		}
	}
	return stakes, nil
}

func (m *memory) ReadStakingOnlyNodeIDs(stakeholderAddress string, excludedNodeIDs []string) ([]string, error) {
	return nil, nil
}

func (m *memory) ConfirmStake(transactionHash string) error {
	return nil
}

func (m *memory) ConfirmAdmission(nodeID bson.ObjectId) error {
	return nil
}

func (m *memory) ReadUnconfirmedAdmissions() ([]*Admission, error) {
	return nil, nil
}

func (m *memory) ReadUnconfirmedStakes() ([]*Stake, error) {
	return nil, nil
}
