package main

import (
	"bufio"
	"context"
	"flag"
	"os"
	"regexp"
	"strconv"
	"strings"
	"time"

	"github.com/arpachain/apollo/src/wallet/pkg/eth"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/labstack/gommon/log"
)

var (
	dryRun                         = flag.Bool("dry_run", false, "dry run mode only prints the target addresses and corresponding amounts")
	inputFilePath                  = flag.String("input_file_path", "input.csv", "the file path of the csv that has 2 columns: the first column is target addresses, the second column is the amount, no headers are allowed")
	batchSize                      = flag.Int("batch_size", 10, "the batch size of transactions to prevent jamming the Ethereum")
	batchWaitTime                  = flag.Duration("batch_wait_time", 30*time.Second, "the wait time between each batch of transactions")
	txWaitTime                     = flag.Duration("tx_wait_time", time.Second, "the wait time between each tx in a single batch")
	ethClientURL                   = flag.String("eth_url", "wss://ropsten.infura.io/ws/v3/d89e8976ca54463faef78b643edfdae9", "the url for eth connection")
	privateKeyHex                  = flag.String("private_key", "0E3AF342AD3349DBAF3AEFEA16FEB5368E8AA124B2F13E0563D9B9D8900C9310", "the private key of admission fee account")
	tokenAddress                   = flag.String("token_address", "0xd1e00ce58a12e77e736a6417fb5fe6e0f08697ab", "the erc-20 contract address")
	numConfirmations               = flag.Int("num_confirmations", 8, "number of block confirmation for transaction")
	pollingIntervalForMining       = flag.Duration("interval_mining", 10*time.Second, "polling interval before tx is mined")
	pollingIntervalForConfirmation = flag.Duration("interval_confirm", 30*time.Second, "polling interval before tx is confirmed")
	waitingTime                    = flag.Duration("waiting_time", 60*time.Minute, "total waiting time for tx confirmation")
	infuraHeartbeatInterval        = flag.Duration("infura_heartbeat_interval", 15*time.Minute, "the interval between each heartbeat to prevent infura from dropping the connection")
	infuraHeartbeatRetryInterval   = flag.Duration("infura_retry_interval", time.Minute, "the interval between each retry when infura heartbeat fails")
	infuraHeartbeatRetryTimes      = flag.Int("infura_retry_times", 3, "the number of retries when infura heartbeat fails")
)

func main() {
	flag.Parse()
	re := regexp.MustCompile("^0x[0-9a-fA-F]{40}$")
	privateKey, err := eth.ParsePrivateKeyHex(*privateKeyHex)
	if err != nil {
		log.Fatal(err.Error())
	}
	file, err := os.Open(*inputFilePath)
	if err != nil {
		log.Fatal(err)
	}
	scanner := bufio.NewScanner(file)
	ethClient, err := ethclient.Dial(*ethClientURL)
	if err != nil {
		log.Fatal(err)
	}
	if !re.MatchString(*tokenAddress) {
		log.Fatalf("invalid token address: %v", *tokenAddress)
	}
	address := common.HexToAddress(*tokenAddress)
	bytecode, err := ethClient.CodeAt(context.Background(), address, nil) // nil is latest block
	isContract := len(bytecode) > 0
	if err != nil {
		log.Fatal(err)
	}
	if !isContract {
		log.Fatalf("invalid contract address: %v", *tokenAddress)
	}
	tokenClient := eth.NewARPAClient(
		ethClient,
		*tokenAddress,
		int64(*numConfirmations),
		*pollingIntervalForMining,
		*pollingIntervalForConfirmation,
		*waitingTime,
	)
	tokenClient.StartInfuraHeartbeat(
		*infuraHeartbeatInterval,
		*infuraHeartbeatRetryInterval,
		*infuraHeartbeatRetryTimes,
	)
	counter := 1
	for scanner.Scan() {
		if err := scanner.Err(); err != nil {
			log.Fatal(err)
		}
		line := scanner.Text()
		row := strings.Split(line, ",")
		toAddress := row[0]
		if !re.MatchString(toAddress) {
			log.Errorf("invalid recipient address: %v", toAddress)
			continue
		}
		amount := row[1]
		amountInt64, err := strconv.ParseInt(amount, 10, 64)
		if err != nil {
			log.Errorf("invalid amount: %v, error: %v", amount, err.Error())
			continue
		}
		log.Infof("sending %v tokens to %v", amountInt64, toAddress)
		if !*dryRun {
			txHash, err := tokenClient.SendToken(context.Background(), toAddress, amountInt64, privateKey)
			if err != nil {
				log.Errorf("sending token failed: %v", err.Error())
				continue
			}
			log.Infof("sent %v to %v with tx hash: %v", amountInt64, toAddress, txHash)
		} else {
			log.Infof("sent %v to %v with tx hash: %v", amountInt64, toAddress, "dry-run")
		}
		if counter%*batchSize == 0 {
			// sleep in between each batch of multiple transactions
			// so it does not jam
			time.Sleep(*batchWaitTime)
		} else {
			// sleep in between each transaction in one single batch
			time.Sleep(*txWaitTime)
		}
		counter++
	}
}
