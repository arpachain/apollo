package config

import "time"

const (
	EighteenZeros = "000000000000000000"
)

type NodeConfig struct {
	StakeLockingPeriod           time.Duration
	AdmissionFee                 string
	MinimumStakingAmount         string
	ProfitShare                  float32
	StageOneMinReturnThreshold   string
	StageOneMaxReturnThreshold   string
	StageTwoMinReturnThreshold   string
	StageTwoMaxReturnThreshold   string
	StageThreeMinReturnThreshold string
	StageThreeMaxReturnThreshold string
	StageOneReturnRate           float32
	StageTwoReturnRate           float32
	StageThreeReturnRate         float32
	DividendInterval             time.Duration
}
