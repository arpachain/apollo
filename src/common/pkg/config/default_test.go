package config_test

import (
	"testing"

	"github.com/arpachain/apollo/src/common/pkg/config"
	"github.com/stretchr/testify/assert"
)

func TestGetTotalThreshold(t *testing.T) {
	assert := assert.New(t)
	defaultConfig := config.NewDefaultConfig(false, "fake_key")
	testNodeConfig := defaultConfig.GetNodeConfigByType(config.SuperNode)
	t.Logf("AdmissionFee: %v", testNodeConfig.AdmissionFee+config.EighteenZeros)
	t.Logf("MinimumStakingAmount: %v", testNodeConfig.MinimumStakingAmount+config.EighteenZeros)
	threshold, err := defaultConfig.GetTotalThreshold(testNodeConfig)
	t.Logf("Threshold: %v", threshold.String())
	assert.NoError(err)
}
