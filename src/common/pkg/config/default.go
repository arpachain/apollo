package config

import (
	"fmt"
	"math/big"
	"time"

	"github.com/labstack/gommon/log"
)

type DefaultConfig interface {
	GetAdmissionFee(nodeConfig NodeConfig) (*big.Int, error)
	GetMinimumStakingAmount(nodeConfig NodeConfig) (*big.Int, error)
	GetTotalThreshold(nodeConfig NodeConfig) (*big.Int, error)
	GetNodeConfigByType(nodeType NodeType) NodeConfig
	GetAdmissionAddress() string
	GetFissionNodeMinStakingAmtStr() string
	GetFissionNodeMinStakingAmt() (*big.Int, error)
	GetFissionNodeReturnRate() float32
	GetReturnRate(nodeType NodeType, stakingAmount *big.Int) float32
	GetMinStakingAmount() (*big.Int, error)
}

type defaultConfig struct {
	configMap                   map[NodeType]NodeConfig
	admissionAddress            string
	fissionNodeMinStakingAmount string
	fissionNodeReturnRate       float32
	minStakingAmount            string
}

func NewDefaultConfig(isProd bool, admissionAddress string) DefaultConfig {
	configMap := map[NodeType]NodeConfig{}
	var fissionNodeMinStakingAmount string
	var fissionNodeReturnRate float32
	var minStakingAmount string
	if isProd {
		configMap[SuperNode] = getSuperNodeConfig()
		configMap[NormalNode] = getNormalNodeConfig()
		fissionNodeMinStakingAmount = "5000"
		fissionNodeReturnRate = 0.16
		minStakingAmount = "1000"
	} else {
		configMap[SuperNode] = getSuperNodeConfigDev()
		configMap[NormalNode] = getNormalNodeConfigDev()
		fissionNodeMinStakingAmount = "5"
		fissionNodeReturnRate = 0.1
		minStakingAmount = "1"
	}
	return &defaultConfig{
		configMap:                   configMap,
		admissionAddress:            admissionAddress,
		fissionNodeMinStakingAmount: fissionNodeMinStakingAmount,
		fissionNodeReturnRate:       fissionNodeReturnRate,
		minStakingAmount:            minStakingAmount,
	}
}

func getSuperNodeConfig() NodeConfig {
	return NodeConfig{
		StakeLockingPeriod:           4380 * time.Hour, // 4380 hours is approximately 6 months
		AdmissionFee:                 "10000",
		MinimumStakingAmount:         "990000",
		ProfitShare:                  0.1,
		StageOneMinReturnThreshold:   "990000",
		StageOneMaxReturnThreshold:   "2000000",
		StageTwoMinReturnThreshold:   "2000000",
		StageTwoMaxReturnThreshold:   "5000000",
		StageThreeMinReturnThreshold: "5000000",
		StageThreeMaxReturnThreshold: "10000000",
		StageOneReturnRate:           0.24,
		StageTwoReturnRate:           0.26,
		StageThreeReturnRate:         0.30,
		DividendInterval:             720 * time.Hour, // 720 hours is approximately 1 month (30 days)
	}
}

func getNormalNodeConfig() NodeConfig {
	return NodeConfig{
		StakeLockingPeriod:           4380 * time.Hour, // 4380 hours is approximately 6 months
		AdmissionFee:                 "2000",
		MinimumStakingAmount:         "148000",
		ProfitShare:                  0.1,
		StageOneMinReturnThreshold:   "148000",
		StageOneMaxReturnThreshold:   "1000000",
		StageTwoMinReturnThreshold:   "1000000",
		StageTwoMaxReturnThreshold:   "2000000",
		StageThreeMinReturnThreshold: "2000000",
		StageThreeMaxReturnThreshold: "10000000",
		StageOneReturnRate:           0.20,
		StageTwoReturnRate:           0.22,
		StageThreeReturnRate:         0.24,
		DividendInterval:             720 * time.Hour, // 720 hours is approximately 1 month (30 days)
	}
}

func (c *defaultConfig) GetAdmissionFee(nodeConfig NodeConfig) (*big.Int, error) {
	admissionFee := new(big.Int)
	admissionFee, ok := admissionFee.SetString(nodeConfig.AdmissionFee+EighteenZeros, 10)
	if !ok {
		return nil, fmt.Errorf("big.Int SetString failed")
	}
	return admissionFee, nil
}

func (c *defaultConfig) GetMinimumStakingAmount(nodeConfig NodeConfig) (*big.Int, error) {
	minimumStakingAmount := new(big.Int)
	minimumStakingAmount, ok := minimumStakingAmount.SetString(nodeConfig.MinimumStakingAmount+EighteenZeros, 10)
	if !ok {
		return nil, fmt.Errorf("big.Int SetString failed")
	}
	return minimumStakingAmount, nil
}

func (c *defaultConfig) GetMinStakingAmount() (*big.Int, error) {
	minimumStakingAmount := new(big.Int)
	minimumStakingAmount, ok := minimumStakingAmount.SetString(c.minStakingAmount+EighteenZeros, 10)
	if !ok {
		return nil, fmt.Errorf("big.Int SetString failed")
	}
	return minimumStakingAmount, nil
}

func (c *defaultConfig) GetTotalThreshold(nodeConfig NodeConfig) (*big.Int, error) {
	admissionFee, err := c.GetAdmissionFee(nodeConfig)
	if err != nil {
		return nil, err
	}
	minimumStakingAmount, err := c.GetMinimumStakingAmount(nodeConfig)
	if err != nil {
		return nil, err
	}
	sum := new(big.Int)
	return sum.Add(admissionFee, minimumStakingAmount), nil
}

func (c *defaultConfig) GetNodeConfigByType(nodeType NodeType) NodeConfig {
	switch nodeType {
	case SuperNode:
		return c.configMap[SuperNode]
	case NormalNode:
		return c.configMap[NormalNode]
	default:
		return c.configMap[NormalNode]
	}
}

func (c *defaultConfig) GetAdmissionAddress() string {
	return c.admissionAddress
}

func (c *defaultConfig) GetFissionNodeMinStakingAmt() (*big.Int, error) {
	minimumStakingAmount := new(big.Int)
	minimumStakingAmount, ok := minimumStakingAmount.SetString(c.fissionNodeMinStakingAmount+EighteenZeros, 10)
	if !ok {
		return nil, fmt.Errorf("big.Int SetString failed")
	}
	return minimumStakingAmount, nil
}

func (c *defaultConfig) GetFissionNodeMinStakingAmtStr() string {
	return c.fissionNodeMinStakingAmount
}

func (c *defaultConfig) GetFissionNodeReturnRate() float32 {
	return c.fissionNodeReturnRate
}

func (c *defaultConfig) GetReturnRate(nodeType NodeType, stakingAmount *big.Int) float32 {
	nodeConfig := c.GetNodeConfigByType(nodeType)
	stageOneMin := new(big.Int)
	stageOneMin, ok := stageOneMin.SetString(nodeConfig.StageOneMinReturnThreshold+EighteenZeros, 10)
	if !ok {
		log.Fatalf("big.Int SetString failed")
	}
	stageOneMax := new(big.Int)
	stageOneMax, ok = stageOneMax.SetString(nodeConfig.StageOneMaxReturnThreshold+EighteenZeros, 10)
	if !ok {
		log.Fatalf("big.Int SetString failed")
	}
	stageTwoMin := new(big.Int)
	stageTwoMin, ok = stageTwoMin.SetString(nodeConfig.StageTwoMinReturnThreshold+EighteenZeros, 10)
	if !ok {
		log.Fatalf("big.Int SetString failed")
	}
	stageTwoMax := new(big.Int)
	stageTwoMax, ok = stageTwoMax.SetString(nodeConfig.StageTwoMaxReturnThreshold+EighteenZeros, 10)
	if !ok {
		log.Fatalf("big.Int SetString failed")
	}
	stageThreeMin := new(big.Int)
	stageThreeMin, ok = stageThreeMin.SetString(nodeConfig.StageThreeMinReturnThreshold+EighteenZeros, 10)
	if !ok {
		log.Fatalf("big.Int SetString failed")
	}
	stageThreeMax := new(big.Int)
	stageThreeMax, ok = stageThreeMax.SetString(nodeConfig.StageThreeMaxReturnThreshold+EighteenZeros, 10)
	if !ok {
		log.Fatalf("big.Int SetString failed")
	}
	if stakingAmount.Cmp(stageOneMin) >= 0 && stakingAmount.Cmp(stageOneMax) == -1 {
		return nodeConfig.StageOneReturnRate
	}
	if stakingAmount.Cmp(stageTwoMin) >= 0 && stakingAmount.Cmp(stageTwoMax) == -1 {
		return nodeConfig.StageTwoReturnRate
	}
	if stakingAmount.Cmp(stageThreeMin) >= 0 && stakingAmount.Cmp(stageThreeMax) == -1 {
		return nodeConfig.StageThreeReturnRate
	}
	if stakingAmount.Cmp(stageThreeMax) >= 0 {
		return nodeConfig.StageThreeReturnRate
	}
	return 0
}
