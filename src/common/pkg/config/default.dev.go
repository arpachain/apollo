package config

import (
	"time"
)

func getSuperNodeConfigDev() NodeConfig {
	return NodeConfig{
		StakeLockingPeriod:           300 * time.Second,
		AdmissionFee:                 "1",
		MinimumStakingAmount:         "2",
		ProfitShare:                  0.1,
		StageOneMinReturnThreshold:   "2",
		StageOneMaxReturnThreshold:   "3",
		StageTwoMinReturnThreshold:   "3",
		StageTwoMaxReturnThreshold:   "4",
		StageThreeMinReturnThreshold: "4",
		StageThreeMaxReturnThreshold: "5",
		StageOneReturnRate:           0.11,
		StageTwoReturnRate:           0.13,
		StageThreeReturnRate:         0.15,
		DividendInterval:             10 * time.Second,
	}
}

func getNormalNodeConfigDev() NodeConfig {
	return NodeConfig{
		StakeLockingPeriod:           300 * time.Second,
		AdmissionFee:                 "1",
		MinimumStakingAmount:         "2",
		ProfitShare:                  0.1,
		StageOneMinReturnThreshold:   "2",
		StageOneMaxReturnThreshold:   "3",
		StageTwoMinReturnThreshold:   "3",
		StageTwoMaxReturnThreshold:   "4",
		StageThreeMinReturnThreshold: "4",
		StageThreeMaxReturnThreshold: "5",
		StageOneReturnRate:           0.10,
		StageTwoReturnRate:           0.11,
		StageThreeReturnRate:         0.12,
		DividendInterval:             10 * time.Second,
	}
}
