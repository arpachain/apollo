package config

// NodeType is an enum to represent the type of the node
type NodeType int

const (
	// SuperNode = 0
	SuperNode NodeType = iota
	// NormalNode = 1
	NormalNode
)
