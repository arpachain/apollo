package main

import (
	"flag"

	"github.com/arpachain/apollo/src/common/pkg/config"
	"github.com/arpachain/apollo/src/rest/pkg/setup"
	"github.com/arpachain/apollo/src/return/pkg/claim"
	"github.com/arpachain/apollo/src/staking/pkg/stake"
	"github.com/arpachain/apollo/src/staking/pkg/stakeholder"
	"github.com/labstack/gommon/log"
)

var (
	isProd                = flag.Bool("is_prod", false, "indicate whether to use dev config")
	dryrun                = flag.Bool("dryrun", true, "indicates whether writing the snapshot to the db")
	dbServerIP            = flag.String("db_ip", "localhost", "the ip of database server, dev environment will ignore this option")
	neo4jConnectionString = flag.String("neo4j_connection_string", "bolt://localhost:7687", "connection string of neo4j database")
	neo4jUsername         = flag.String("neo4j_username", "neo4j", "username for neo4j")
	neo4jPassword         = flag.String("neo4j_password", "neo4j", "password for neo4j")
	passphrase            = flag.String("passphrase", "default_passphrase", "the passphrase used to encrypt keys")
)

func main() {
	flag.Parse()
	config := config.NewDefaultConfig(*isProd, "admissionAddress")
	db := setup.CreateMongoDBSession(*dbServerIP)
	stakeholderDB := stakeholder.NewMongo(db)
	stakeDB := stake.NewMongo(db)
	dividendDB := claim.NewMongo(db)
	eod := claim.NewEOD(
		config,
		stakeDB,
		stakeholderDB,
		dividendDB,
	)
	err := eod.TakeSnapshot(*dryrun)
	if err != nil {
		log.Error(err.Error())
	}
}
