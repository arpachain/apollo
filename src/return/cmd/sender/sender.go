package main

import (
	"context"
	"flag"
	"strconv"
	"strings"
	"time"

	"github.com/arpachain/apollo/src/common/pkg/config"
	"github.com/arpachain/apollo/src/rest/pkg/setup"
	"github.com/arpachain/apollo/src/return/pkg/claim"
	"github.com/arpachain/apollo/src/staking/pkg/stake"
	"github.com/arpachain/apollo/src/staking/pkg/stakeholder"
	"github.com/arpachain/apollo/src/wallet/pkg/eth"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/labstack/gommon/log"
)

var (
	isProd                         = flag.Bool("is_prod", false, "indicate whether to use dev config")
	dryrun                         = flag.Bool("dryrun", true, "indicates whether writing the snapshot to the db")
	dbServerIP                     = flag.String("db_ip", "localhost", "the ip of database server, dev environment will ignore this option")
	neo4jConnectionString          = flag.String("neo4j_connection_string", "bolt://localhost:7687", "connection string of neo4j database")
	neo4jUsername                  = flag.String("neo4j_username", "neo4j", "username for neo4j")
	neo4jPassword                  = flag.String("neo4j_password", "neo4j", "password for neo4j")
	passphrase                     = flag.String("passphrase", "default_passphrase", "the passphrase used to encrypt keys")
	ethClientURL                   = flag.String("eth_url", "wss://ropsten.infura.io/ws/v3/d89e8976ca54463faef78b643edfdae9", "the url for eth connection")
	admissionPrivateKeyHex         = flag.String("admission_private_key", "0E3AF342AD3349DBAF3AEFEA16FEB5368E8AA124B2F13E0563D9B9D8900C9310", "the private key of admission fee account")
	arpaTokenAddress               = flag.String("token_address", "0xd1e00ce58a12e77e736a6417fb5fe6e0f08697ab", "the erc-20 contract address")
	numConfirmations               = flag.Int("num_confirmations", 8, "number of block confirmation for transaction")
	pollingIntervalForMining       = flag.Duration("interval_mining", 10*time.Second, "polling interval before tx is mined")
	pollingIntervalForConfirmation = flag.Duration("interval_confirm", 30*time.Second, "polling interval before tx is confirmed")
	waitingTime                    = flag.Duration("waiting_time", 60*time.Minute, "total waiting time for tx confirmation")
	infuraHeartbeatInterval        = flag.Duration("infura_heartbeat_interval", 15*time.Minute, "the interval between each heartbeat to prevent infura from dropping the connection")
	infuraHeartbeatRetryInterval   = flag.Duration("infura_retry_interval", time.Minute, "the interval between each retry when infura heartbeat fails")
	infuraHeartbeatRetryTimes      = flag.Int("infura_retry_times", 3, "the number of retries when infura heartbeat fails")
)

const (
	EighteenZeros = "000000000000000000"
)

func main() {
	flag.Parse()
	config := config.NewDefaultConfig(*isProd, "admissionAddress")
	db := setup.CreateMongoDBSession(*dbServerIP)
	stakeholderDB := stakeholder.NewMongo(db)
	stakeDB := stake.NewMongo(db)
	dividendDB := claim.NewMongo(db)
	eod := claim.NewEOD(
		config,
		stakeDB,
		stakeholderDB,
		dividendDB,
	)
	ethClient, err := ethclient.Dial(*ethClientURL)
	if err != nil {
		log.Fatalf("ethereum client connection failed: %v", err)
	}
	arpaClient := eth.NewARPAClient(
		ethClient,
		*arpaTokenAddress,
		int64(*numConfirmations),
		*pollingIntervalForMining,
		*pollingIntervalForConfirmation,
		*waitingTime,
	)
	arpaClient.StartInfuraHeartbeat(
		*infuraHeartbeatInterval,
		*infuraHeartbeatRetryInterval,
		*infuraHeartbeatRetryTimes,
	)
	allNodes, err := stakeholderDB.ReadAllNodes()
	if err != nil {
		log.Fatalf("getting nodes failed: %v", err.Error())
	}
	privateKey, err := eth.ParsePrivateKeyHex(*admissionPrivateKeyHex)
	if err != nil {
		log.Fatal(err.Error())
	}
	for _, node := range allNodes {
		if node.Activated && node.Votable {
			allStakes, err := stakeDB.ReadAllStakes(node.ID)
			if err != nil {
				log.Errorf("getting stakes failed: %v", err.Error())
			}
			totalStakingAmount, err := eod.GetTotalStakingAmount(node.ID)
			if err != nil {
				log.Errorf("getting total amount failed for node id:%v for reason: %v", node.ID.Hex(), err.Error())
			}
			log.Infof("total staking amount is %v", totalStakingAmount)
			ownerReturnRate := float64(config.GetReturnRate(node.Type, totalStakingAmount) / 12)
			stakerReturnRate := float64(config.GetReturnRate(node.Type, totalStakingAmount) * 0.9 / 12)
			ownerRewardRate := float64(config.GetReturnRate(node.Type, totalStakingAmount) * 0.1 / 12)
			for _, stake := range allStakes {
				if !stake.Confirmed {
					log.Infof("skipping unconfirmed stake")
					continue
				}
				var returnRate float64
				if stake.StakeholderAddress == node.StakeholderAddress {
					returnRate = ownerReturnRate
				} else {
					returnRate = stakerReturnRate
				}
				log.Infof("monthly owner return rate is: %v", ownerReturnRate)
				log.Infof("annual owner return rate is: %v", ownerReturnRate*12)
				log.Infof("monthly staker return rate is: %v", stakerReturnRate)
				log.Infof("annual staker return rate is: %v", stakerReturnRate*12)
				log.Infof("monthly reward return rate is: %v", ownerRewardRate)
				log.Infof("annual reward return rate is: %v", ownerRewardRate*12)
				amount := strings.TrimSuffix(stake.Amount, EighteenZeros)
				log.Infof("staking amount: %v", amount)
				amountFloat, err := strconv.ParseFloat(amount, 64)
				if err != nil {
					log.Error(err.Error())
				}
				returnAmount := amountFloat * returnRate

				if *dryrun {
					log.Infof("sent %f return to %v in dryrun mode", returnAmount, stake.StakeholderAddress)
					if stake.StakeholderAddress != node.StakeholderAddress {
						log.Infof("sent %f reward to %v in dryrun mode", amountFloat*ownerRewardRate, node.StakeholderAddress)
					}
				} else {
					var amount int64
					if *isProd == false {
						amount = int64(1)
					} else {
						amount = int64(returnAmount)
					}
					if amount >= 1 {
						log.Infof("sending %f return to %v", amount, stake.StakeholderAddress)
						txHash, err := arpaClient.SendToken(context.Background(), stake.StakeholderAddress, amount, privateKey)
						if err != nil {
							log.Errorf("sending token failed: %v", err.Error())
							continue
						}
						log.Infof("RETURN: sent %f to %v with tx hash: %v", amount, stake.StakeholderAddress, txHash)
						time.Sleep(time.Second * 1)
					}
					if stake.StakeholderAddress != node.StakeholderAddress {
						var rewardAmount int64
						if *isProd == false {
							rewardAmount = int64(1)
						} else {
							rewardAmount = int64(amountFloat * ownerRewardRate)
						}
						if rewardAmount >= 1 {
							log.Infof("sending %f reward to %v", rewardAmount, node.StakeholderAddress)
							txHash, err := arpaClient.SendToken(context.Background(), node.StakeholderAddress, rewardAmount, privateKey)
							if err != nil {
								log.Errorf("sending token failed: %v", err.Error())
								continue
							}
							log.Infof("REWARD: sent %f to %v with tx hash: %v", rewardAmount, node.StakeholderAddress, txHash)
						}
					}
					time.Sleep(time.Second * 1)
				}
			}
		}
	}
}
