package main

import (
	"flag"
	"fmt"

	"github.com/arpachain/apollo/src/common/pkg/config"
	"github.com/arpachain/apollo/src/rest/pkg/setup"
	"github.com/arpachain/apollo/src/return/pkg/withdraw"
	"github.com/arpachain/apollo/src/staking/pkg/stakeholder"
)

var (
	isProd             = flag.Bool("is_prod", false, "indicate whether to use dev config")
	dbServerIP         = flag.String("db_ip", "localhost", "the ip of database server, dev environment will ignore this option")
	stakeholderAddress = flag.String("stakeholderAddress", "test_holder_address", "The testing token holder address to create or locate a stake holder")
	passphrase         = flag.String("passphrase", "default_passphrase", "The passphrase used to encrypt and decrypt keys")
)

func main() {
	flag.Parse()
	var stakeholderDB stakeholder.Database
	if *isProd {
		db := setup.CreateMongoDBSession(*dbServerIP)
		stakeholderDB = stakeholder.NewMongo(db)
	} else {
		stakeholderDB = stakeholder.NewMemory()
	}
	stakeholderManager := stakeholder.NewManager(*passphrase, stakeholderDB)
	if *isProd {
		// Do nothing
	}else{
		_, _, err := stakeholderManager.SetupNewNode("TestNode", *stakeholderAddress, config.NormalNode)
		if err != nil {
			fmt.Printf("Setup node failed for reason: %v", err)
			return
		}
	}

	DecryptorObject := withdraw.NewDecryptor(stakeholderManager)
	res, err := DecryptorObject.DecryptPrivateKeysForStakeholder(*stakeholderAddress, *passphrase)
	if err != nil {
		fmt.Printf("Decrypt private keys for stakeholder failed for reason: %v", err)
	} 
	fmt.Printf("All private key(s) decrypted are as below:\n")
	fmt.Printf("%v\n", res)
	fmt.Printf("Finished successfully.")
	

}
