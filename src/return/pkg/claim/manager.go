package claim

type Manager interface {
	ClaimDividend(address string) error
}

type manager struct {
	database Database
}
