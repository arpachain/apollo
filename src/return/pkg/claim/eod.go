package claim

import (
	"fmt"
	"math/big"
	"strconv"
	"strings"
	"time"

	"github.com/arpachain/apollo/src/common/pkg/config"
	"github.com/arpachain/apollo/src/staking/pkg/stake"
	"github.com/arpachain/apollo/src/staking/pkg/stakeholder"
	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
	"github.com/labstack/gommon/log"
)

const (
	EighteenZeros = "000000000000000000"
)

type EOD struct {
	config           config.DefaultConfig
	stakeDatabase    stake.Database
	nodeDatabase     stakeholder.Database
	dividendDatabase Database
}

func NewEOD(
	config config.DefaultConfig,
	stakeDatabase stake.Database,
	nodeDatabase stakeholder.Database,
	dividendDatabase Database,
) *EOD {
	return &EOD{
		config:           config,
		stakeDatabase:    stakeDatabase,
		nodeDatabase:     nodeDatabase,
		dividendDatabase: dividendDatabase,
	}
}

func (eod *EOD) TakeSnapshot(dryrun bool) error {
	allNodes, err := eod.nodeDatabase.ReadAllNodes()
	if err != nil {
		return err
	}
	for _, node := range allNodes {
		if node.Activated && node.Votable {
			allStakes, err := eod.stakeDatabase.ReadAllStakes(node.ID)
			if err != nil {
				return err
			}
			totalStakingAmount, err := eod.GetTotalStakingAmount(node.ID)
			if err != nil {
				return err
			}
			ownerReturnRate := float64(eod.config.GetReturnRate(node.Type, totalStakingAmount))
			stakerReturnRate := float64(ownerReturnRate * 0.9)
			for _, stake := range allStakes {
				var returnRate float64
				if stake.StakeholderAddress == node.StakeholderAddress {
					returnRate = ownerReturnRate
				} else {
					returnRate = stakerReturnRate
				}
				returnRate = returnRate / 365
				amount := strings.TrimSuffix(stake.Amount, EighteenZeros)
				amountFloat, err := strconv.ParseFloat(amount, 64)
				if err != nil {
					return nil
				}
				dividendAmount := amountFloat * returnRate
				dividendAmountString := fmt.Sprintf("%f", dividendAmount)
				currentDate := time.Now().UTC().Format("01-02-2006") // Ref time: Mon Jan 2 15:04:05 MST 2006
				dividend := &Dividend{
					ClaimerAddress:       stake.StakeholderAddress,
					NodeID:               node.ID,
					StakeTransactionHash: stake.TransactionHash,
					Amount:               dividendAmountString,
					Date:                 currentDate,
					Claimed:              false,
				}
				existingDividend, err := eod.dividendDatabase.ReadDividend(stake.TransactionHash, currentDate)
				if err != nil {
					if err == mgo.ErrNotFound {
						log.Infof("taking snapshot for node:%v, %v * %v = %v", node.Name, amount, returnRate, dividendAmountString)
						if !dryrun {
							err = eod.dividendDatabase.CreateDividend(dividend)
							if err != nil {
								return err
							}
						}
					}
				} else {
					log.Warnf("a snapshot has already been taken for tx: %v on %v\n", existingDividend.StakeTransactionHash, existingDividend.Date)
				}
			}
		}
	}
	return nil
}

func (eod *EOD) GetTotalStakingAmount(nodeID bson.ObjectId) (*big.Int, error) {
	stakes, err := eod.stakeDatabase.ReadAllStakes(nodeID)
	if err != nil {
		return nil, err
	}
	totalAmount := new(big.Int)
	for _, stake := range stakes {
		amount := new(big.Int)
		amount, _ = amount.SetString(stake.Amount, 10)
		totalAmount = totalAmount.Add(totalAmount, amount)
	}
	return totalAmount, nil
}
