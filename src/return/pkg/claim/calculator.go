package claim

import (
	"github.com/arpachain/apollo/src/staking/pkg/stake"
	"github.com/arpachain/apollo/src/staking/pkg/stakeholder"
)

type Calculator interface {
	CalculateDividendForStakeholder(address string) (string, error)
	CalculateDividendForVoter(address string) (string, error)
}

type calculator struct {
	stakeDatabase stake.Database
	nodeDatabase  stakeholder.Database
}

func NewCalculator(stakeDatabase stake.Database, nodeDatabase stakeholder.Database) Calculator {
	return &calculator{
		stakeDatabase: stakeDatabase,
		nodeDatabase:  nodeDatabase,
	}
}

func (c *calculator) CalculateDividendForStakeholder(address string) (string, error) {
	return "", nil
}

func (c *calculator) CalculateDividendForVoter(address string) (string, error) {
	return "", nil
}
