package claim

type Database interface {
	CreateDividend(dividend *Dividend) error
	ReadDividend(stakeTransactionHash, date string) (*Dividend, error)
}
