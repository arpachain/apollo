package claim

import (
	"github.com/globalsign/mgo/bson"
)

type Dividend struct {
	ClaimerAddress       string        `json:"claimerAddress" bson:"claimerAddress"`
	NodeID               bson.ObjectId `json:"nodeId" bson:"nodeId"`
	StakeTransactionHash string        `json:"stakeTransactionHash" bson:"stakeTransactionHash"`
	Amount               string        `json:"amount" bson:"amount"`
	Date                 string        `json:"date" bson:"date"`
	Claimed              bool          `json:"claimed" bson:"claimed"`
}
