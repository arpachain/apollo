package claim

import (
	"strings"

	"github.com/globalsign/mgo"
	"github.com/globalsign/mgo/bson"
)

type mongo struct {
	DB *mgo.Session
}

func NewMongo(db *mgo.Session) Database {
	return &mongo{
		DB: db,
	}
}

func (m *mongo) CreateDividend(dividend *Dividend) error {
	db := m.DB.Clone()
	defer db.Close()
	dividend.ClaimerAddress = strings.ToLower(dividend.ClaimerAddress)
	dividend.StakeTransactionHash = strings.ToLower(dividend.StakeTransactionHash)
	if err := db.DB("apollo").C("dividends").Insert(dividend); err != nil {
		return err
	}
	return nil
}

func (m *mongo) ReadDividend(stakeTransactionHash, date string) (*Dividend, error) {
	d := new(Dividend)
	db := m.DB.Clone()
	defer db.Close()
	if err := db.DB("apollo").C("dividends").
		Find(bson.M{"stakeTransactionHash": strings.ToLower(stakeTransactionHash), "date": date}).
		One(d); err != nil {
		return nil, err
	}
	return d, nil
}
