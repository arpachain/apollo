package withdraw

import (
	"github.com/arpachain/apollo/src/staking/pkg/stakeholder"
	eth "github.com/arpachain/apollo/src/wallet/pkg/eth"
)

// Decryptor is a object for decrypting encrypted private key in a stake holder address.
type Decryptor interface {
	DecryptPrivateKeysForStakeholder(stakeholderAddress string, passphrase string) ([]string, error)
}

type decryptor struct {
	/*
		passphrase    string
		stakeholderDB stakeholder.Database
	*/
	stakeholderManager stakeholder.Manager
}

// NewDecryptor will create and return a decryptor.
func NewDecryptor(stakeholderManager stakeholder.Manager) Decryptor {
	return &decryptor{
		/*
			passphrase:    passphrase,
			stakeholderDB: stakeholderDB,
		*/
		stakeholderManager: stakeholderManager,
	}
}

// DecryptPrivateKeysForStakeholder is a function that receives stake holder address and passphrase,
// then print out all the private key(s) plaintext of node(s) found in this holder address.
func (m *decryptor) DecryptPrivateKeysForStakeholder(stakeholderAddress string, passphrase string) ([]string, error) {
	nodes, err := m.stakeholderManager.GetNodes(stakeholderAddress)
	if err != nil {
		return nil, err
	}

	plaintextPrivateKeys := make([]string, 0)
	for _, node := range nodes {
		decryptedPrivateKeyHex, err := eth.DecryptKeyHex(node.PrivateKey, passphrase)
		if err != nil {
			errMessage := "FAILED:" + stakeholderAddress + "failed for reason: " + err.Error()
			plaintextPrivateKeys = append(plaintextPrivateKeys, errMessage)
		}
		plaintextPrivateKeys = append(plaintextPrivateKeys, decryptedPrivateKeyHex)
	}

	return plaintextPrivateKeys, err
}
