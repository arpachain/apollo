package eth

import (
	"context"
	"crypto/ecdsa"
	"errors"
	"math/big"
	"strconv"
	"time"

	"github.com/labstack/gommon/log"

	"github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/core/types"
	"github.com/ethereum/go-ethereum/crypto"
	"golang.org/x/crypto/sha3"
)

func (c *arpaClient) checkMined(ctx context.Context,
	pollingIntervalForMining time.Duration,
	txHash common.Hash,
) (*big.Int, error) {
	numRetries := int(c.waitingTime / pollingIntervalForMining)
	if numRetries == 0 {
		numRetries = 1 // at least try once
	}

	for i := 0; i < numRetries; i++ {
		time.Sleep(pollingIntervalForMining)
		receipt, err := c.ethClient.TransactionReceipt(ctx, txHash)
		if err != nil {
			log.Infof("not mined yet for reason: %v, retrying...", err.Error())
			continue
		}
		if receipt.Status != types.ReceiptStatusSuccessful {
			log.Errorf("checking mined failed for reason: %v", err.Error())
			return nil, err
		}
		var header *types.Header
		for _, interval := range c.retryIntervals {
			header, err = c.ethClient.HeaderByNumber(ctx, nil)
			if err == nil {
				log.Infof("found tx: %v in block: %v", txHash.Hex(), receipt.BlockNumber.String())
				log.Infof("current block is: %v", header.Number.String())
				if header.Number.Cmp(receipt.BlockNumber) < 0 {
					continue
				} else {
					return receipt.BlockNumber, nil
				}
			}
			log.Errorf("checking mined failed for reason: %v, retrying...", err.Error())
			time.Sleep(interval)
		}
		log.Errorf("checking mined failed for reason: %v", err.Error())
		return nil, err // failed to get header
	}
	log.Errorf("checking mined timed out\n")
	return nil, errors.New("Timeout when confirming tx mining")
}

// check if the tx received enough confirmations
func (c *arpaClient) checkConfirmation(ctx context.Context,
	pollingIntervalForConfirmation time.Duration,
	minedBlockNum *big.Int,
	numConfirmations int64,
	txHash common.Hash,
) error {
	for {
		time.Sleep(pollingIntervalForConfirmation)
		var currentHeader *types.Header
		var err error
		for _, interval := range c.retryIntervals {
			currentHeader, err = c.ethClient.HeaderByNumber(ctx, nil)
			if err == nil {
				break
			}
			log.Errorf("checking confirmation failed for reason: %v, retrying...", err.Error())
			time.Sleep(interval)
		}
		if err != nil {
			log.Errorf("checking confirmation failed for reason: %v", err.Error())
			return err
		}

		currentBlockNum := currentHeader.Number
		height := big.NewInt(0).Sub(currentBlockNum, minedBlockNum).Int64()
		if height < numConfirmations {
			log.Infof("not enough confirmations, waiting for more...")
			continue
		}

		// to confirm that the tx is not rolled back
		for _, interval := range c.retryIntervals {
			_, err = c.ethClient.TransactionReceipt(ctx, txHash)
			if err == nil {
				break
			}
			log.Errorf("checking confirmation failed for reason: %v, retrying...", err.Error())
			time.Sleep(interval)
		}
		return err
	}
}

func (c *arpaClient) generateKeccak256ForFnSignature() []byte {
	transferFnSignature := []byte("transfer(address,uint256)")
	hash := sha3.NewLegacyKeccak256()
	hash.Write(transferFnSignature)
	methodID := hash.Sum(nil)[:4]
	return methodID
}

func (c *arpaClient) padAmount(amount int64) []byte {
	eighteenDecimal := "000000000000000000" // Yeah right, count it!
	amountStr := strconv.Itoa(int(amount))
	preciseAmount := new(big.Int)
	preciseAmount.SetString(amountStr+eighteenDecimal, 10)
	paddedAmount := common.LeftPadBytes(preciseAmount.Bytes(), 32)
	return paddedAmount
}

func (c *arpaClient) padAddress(toAddress string) []byte {
	toAddressBytes := common.HexToAddress(toAddress)
	paddedAddress := common.LeftPadBytes(toAddressBytes.Bytes(), 32)
	return paddedAddress
}

func (c *arpaClient) compileTransactionData(methodID, paddedAddress, paddedAmount []byte) []byte {
	var data []byte
	data = append(data, methodID...)
	data = append(data, paddedAddress...)
	data = append(data, paddedAmount...)
	return data
}

func (c *arpaClient) getGasLimit(ctx context.Context, transactionData []byte, toAddress string) (uint64, error) {
	toAddressCommon := common.HexToAddress(toAddress)
	gasLimit, err := c.ethClient.EstimateGas(ctx, ethereum.CallMsg{
		To:   &toAddressCommon,
		Data: transactionData,
	})
	if err != nil {
		return 0, err
	}
	return gasLimit * 10, nil
}

func (c *arpaClient) generateTransaction(ctx context.Context,
	toAddress string,
	privateKey *ecdsa.PrivateKey,
	transactionData []byte,
) (*types.Transaction, error) {
	publicKeyECDSA, err := DerivePublicKey(privateKey)
	if err != nil {
		return nil, err
	}
	value := big.NewInt(0) // in wei (0 eth)
	gasPrice, err := c.ethClient.SuggestGasPrice(ctx)
	if err != nil {
		return nil, err
	}
	fromAddress := crypto.PubkeyToAddress(*publicKeyECDSA)
	nonce, err := c.ethClient.PendingNonceAt(ctx, fromAddress)
	if err != nil {
		return nil, err
	}
	gasLimit, err := c.getGasLimit(ctx, transactionData, toAddress)
	if err != nil {
		return nil, err
	}
	transaction := types.NewTransaction(nonce, c.arpaTokenAddress, value, gasLimit, gasPrice.Mul(gasPrice, big.NewInt(10)), transactionData)
	return transaction, nil
}

func (c *arpaClient) signTransaction(ctx context.Context,
	transaction *types.Transaction,
	privateKey *ecdsa.PrivateKey,
) (*types.Transaction, error) {
	chainID, err := c.ethClient.NetworkID(ctx)
	if err != nil {
		return nil, err
	}
	signedTx, err := types.SignTx(transaction, types.NewEIP155Signer(chainID), privateKey)
	if err != nil {
		return nil, err
	}
	return signedTx, nil
}
