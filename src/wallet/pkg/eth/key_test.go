package eth_test

import (
	"testing"

	eth "github.com/arpachain/apollo/src/wallet/pkg/eth"
	"github.com/stretchr/testify/assert"
)

const (
	testPassphrase = "test_passphrase"
)

func TestAll(t *testing.T) {
	assert := assert.New(t)
	account, err := eth.GenerateAccount(testPassphrase)
	assert.NoError(err)
	assert.NotEmpty(account)
	decryptedPrivateKeyHex, err := eth.DecryptKeyHex(account.PrivateKey, testPassphrase)
	assert.NoError(err)
	privateKey, err := eth.ParsePrivateKeyHex(decryptedPrivateKeyHex)
	assert.NoError(err)
	assert.NotNil(privateKey)
	t.Logf("address: %v", account.Address)
}
