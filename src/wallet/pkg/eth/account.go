package eth

// Account is a model for a single Ethereum account
// the PrivateKey is encrypted
type Account struct {
	PrivateKey []byte `json:"-" bson:"privateKey"`
	PublicKey  string `json:"publicKey" bson:"publicKey"`
	Address    string `json:"address" bson:"address"`
}
