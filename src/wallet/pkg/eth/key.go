package eth

import (
	"crypto/ecdsa"
	"fmt"

	"github.com/arpachain/apollo/src/wallet/pkg/util"
	"github.com/ethereum/go-ethereum/common/hexutil"
	"github.com/ethereum/go-ethereum/crypto"
)

func GenerateAccount(passphrase string) (*Account, error) {
	privateKey, err := crypto.GenerateKey()
	if err != nil {
		return nil, err
	}
	privateKeyHex := ConvertPrivateKeyToHex(privateKey)
	publicKey, err := DerivePublicKey(privateKey)
	if err != nil {
		return nil, err
	}
	publicKeyHex := ConvertPublicKeyToHex(publicKey)
	address := DeriveAddress(publicKey)
	encryptedPrivateKeyHex, err := EncryptKeyHex(privateKeyHex, passphrase)
	if err != nil {
		return nil, err
	}
	return &Account{
		Address:    address,
		PrivateKey: encryptedPrivateKeyHex,
		PublicKey:  publicKeyHex,
	}, nil
}

func EncryptKeyHex(keyHex, passphrase string) ([]byte, error) {
	encryptedKeyBytes, err := util.Encrypt([]byte(keyHex), passphrase)
	if err != nil {
		return nil, err
	}
	return encryptedKeyBytes, nil
}

func DecryptKeyHex(encryptedKeyHex []byte, passphrase string) (string, error) {
	decryptedKeyBytes, err := util.Decrypt(encryptedKeyHex, passphrase)
	if err != nil {
		return "", err
	}
	return string(decryptedKeyBytes), nil
}

func ConvertPrivateKeyToHex(privateKey *ecdsa.PrivateKey) string {
	privateKeyBytes := crypto.FromECDSA(privateKey)
	return fmt.Sprint(hexutil.Encode(privateKeyBytes)[2:])
}

func DerivePublicKey(privateKey *ecdsa.PrivateKey) (*ecdsa.PublicKey, error) {
	publicKey := privateKey.Public()
	publicKeyECDSA, ok := publicKey.(*ecdsa.PublicKey)
	if !ok {
		return nil, fmt.Errorf("error casting public key to ECDSA")
	}
	return publicKeyECDSA, nil
}

func DeriveAddress(publicKey *ecdsa.PublicKey) string {
	return crypto.PubkeyToAddress(*publicKey).Hex()
}

func ConvertPublicKeyToHex(publicKey *ecdsa.PublicKey) string {
	publicKeyBytes := crypto.FromECDSAPub(publicKey)
	return fmt.Sprint(hexutil.Encode(publicKeyBytes)[4:])
}

func ParsePrivateKeyHex(keyHex string) (*ecdsa.PrivateKey, error) {
	return crypto.HexToECDSA(keyHex)
}
