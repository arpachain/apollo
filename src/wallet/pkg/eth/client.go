package eth

import (
	"context"
	"crypto/ecdsa"
	"fmt"
	"math/big"
	"strings"
	"time"

	"github.com/labstack/gommon/log"

	"github.com/arpachain/apollo/src/wallet/pkg/abstraction"
	"github.com/arpachain/apollo/src/wallet/pkg/token"
	"github.com/ethereum/go-ethereum"
	"github.com/ethereum/go-ethereum/accounts/abi"
	"github.com/ethereum/go-ethereum/common"
	"github.com/ethereum/go-ethereum/crypto"
)

// Client interface defines crucial functionalities that
// the staking, voting, and reward modules need
type Client interface {
	CheckTransactionConfirmation(
		ctx context.Context,
		transactionHash string,
	) (bool, *big.Int)
	SendToken(
		ctx context.Context,
		toAddress string,
		amount int64,
		privateKey *ecdsa.PrivateKey,
	) (string, error)
	ReadEventTransfer(
		ctx context.Context,
		transactionHash string,
		blockNumber *big.Int,
	) (*EventTransfer, error)
	ConfirmTransfer(
		ctx context.Context,
		address string,
		amount *big.Int,
		transactionHash string,
	) (bool, error)
	StartInfuraHeartbeat(
		infuraHeartbeatInterval time.Duration,
		infuraHeartbeatRetryInterval time.Duration,
		infuraHeartbeatRetryTimes int,
	)
	BackfillConfirmTransaction(
		stakeholderAddress string,
		amount string,
		transactionHash string,
	) error
}

type arpaClient struct {
	ethClient                      abstraction.EthereumClient
	arpaTokenAddress               common.Address
	waitingTime                    time.Duration
	retryIntervals                 []time.Duration
	numConfirmations               int64
	pollingIntervalForMining       time.Duration
	pollingIntervalForConfirmation time.Duration
}

// NewARPAClient returns an implementation of Client interface for ARPA token
func NewARPAClient(
	ethClient abstraction.EthereumClient,
	arpaTokenAddress string,
	numConfirmations int64,
	pollingIntervalForMining time.Duration,
	pollingIntervalForConfirmation time.Duration,
	waitingTime time.Duration,
) Client {
	retryIntervals := []time.Duration{
		34 * time.Second,
		21 * time.Second,
		13 * time.Second,
		8 * time.Second,
		5 * time.Second,
		3 * time.Second,
		2 * time.Second,
		1 * time.Second,
		2 * time.Second,
		3 * time.Second,
		5 * time.Second,
		8 * time.Second,
		13 * time.Second,
		21 * time.Second,
		34 * time.Second,
		55 * time.Second,
		89 * time.Second,
		144 * time.Second,
		233 * time.Second,
		377 * time.Second,
		0 * time.Second, // no need to sleep since the final retry has failed
	}
	return &arpaClient{
		ethClient:                      ethClient,
		arpaTokenAddress:               common.HexToAddress(arpaTokenAddress),
		waitingTime:                    waitingTime,
		retryIntervals:                 retryIntervals,
		numConfirmations:               numConfirmations,
		pollingIntervalForConfirmation: pollingIntervalForConfirmation,
		pollingIntervalForMining:       pollingIntervalForMining,
	}
}

// CheckTransactionConfirmation returns true if the transaction has been confirmed,
// it will also return the block number which the transaction is in
func (c *arpaClient) CheckTransactionConfirmation(
	ctx context.Context,
	transactionHash string,
) (bool, *big.Int) {
	txHash := common.HexToHash(transactionHash)
	minedBlockNum, err := c.checkMined(ctx, c.pollingIntervalForMining, txHash)
	if err != nil {
		log.Errorf("Error: Tx %v failed to get mined for reason: %v\n", txHash.Hex(), err)
		return false, nil
	}
	err = c.checkConfirmation(
		ctx,
		c.pollingIntervalForConfirmation,
		minedBlockNum,
		c.numConfirmations,
		txHash,
	)
	if err != nil {
		log.Errorf("Error: Tx %v failed to get confirmed for reason: %v\n", txHash.Hex(), err)
		return false, nil
	}
	log.Infof("tx %v confirmed\n", txHash.Hex())
	return true, minedBlockNum
}

func (c *arpaClient) SendToken(
	ctx context.Context,
	toAddress string,
	amount int64,
	privateKey *ecdsa.PrivateKey,
) (string, error) {
	methodID := c.generateKeccak256ForFnSignature()
	paddedAddress := c.padAddress(toAddress)
	paddedAmount := c.padAmount(amount)
	transactionData := c.compileTransactionData(methodID, paddedAddress, paddedAmount)
	transaction, err := c.generateTransaction(ctx, toAddress, privateKey, transactionData)
	if err != nil {
		return "", err
	}
	signedTransaction, err := c.signTransaction(ctx, transaction, privateKey)
	if err != nil {
		return "", err
	}
	err = c.ethClient.SendTransaction(ctx, signedTransaction)
	if err != nil {
		return "", err
	}
	return signedTransaction.Hash().Hex(), nil
}

func (c *arpaClient) ReadEventTransfer(
	ctx context.Context,
	transactionHash string,
	blockNumber *big.Int,
) (*EventTransfer, error) {
	fromBlockNumber := new(big.Int)
	fromBlockNumber = fromBlockNumber.Sub(blockNumber, big.NewInt(10))
	toBlockNumber := new(big.Int)
	toBlockNumber = toBlockNumber.Add(blockNumber, big.NewInt(10))
	query := ethereum.FilterQuery{
		FromBlock: fromBlockNumber,
		ToBlock:   toBlockNumber,
		Addresses: []common.Address{
			c.arpaTokenAddress,
		},
	}

	numRetries := int(c.waitingTime / c.pollingIntervalForMining)
	if numRetries == 0 {
		numRetries = 1 // at least try once
	}
	for i := 0; i < numRetries; i++ {
		log.Infof("block#: %v\n", blockNumber)
		logs, err := c.ethClient.FilterLogs(ctx, query)
		if err != nil {
			return nil, err
		}
		contractAbi, err := abi.JSON(strings.NewReader(string(token.TokenABI)))
		if err != nil {
			return nil, err
		}
		logTransferSig := []byte("Transfer(address,address,uint256)")
		logTransferSigHash := crypto.Keccak256Hash(logTransferSig)
		for _, vLog := range logs {
			log.Infof("Log Block Number: %d\n", vLog.BlockNumber)
			log.Infof("Log Index: %d\n", vLog.Index)
			log.Infof("Log Tx Hash: %v\n", vLog.TxHash.Hex())
			log.Infof("Got Tx Hash: %v\n", transactionHash)
			if strings.ToLower(vLog.TxHash.Hex()) == strings.ToLower(transactionHash) {
				log.Infof("Tx found: %v\n", transactionHash)
				switch vLog.Topics[0].Hex() {
				case logTransferSigHash.Hex():
					var event EventTransfer
					err := contractAbi.Unpack(&event, "Transfer", vLog.Data)
					if err != nil {
						return nil, err
					}
					event.From = common.HexToAddress(vLog.Topics[1].Hex())
					event.To = common.HexToAddress(vLog.Topics[2].Hex())
					log.Infof("From: %s\n", event.From.Hex())
					log.Infof("To: %s\n", event.To.Hex())
					log.Infof("Tokens: %s\n", event.Tokens.String())
					return &event, nil
				}
			}
		}
		time.Sleep(c.pollingIntervalForMining)
	}
	return nil, fmt.Errorf("no matching tranfer has been found for tx hash: %v", transactionHash)
}

func (c *arpaClient) ConfirmTransfer(
	ctx context.Context,
	address string,
	amount *big.Int,
	transactionHash string,
) (bool, error) {
	confirmed, blockNumber := c.CheckTransactionConfirmation(
		ctx,
		transactionHash,
	)
	if !confirmed {
		return false, fmt.Errorf("tx: %v is not confirmed", transactionHash)
	}
	eventTransfer, err := c.ReadEventTransfer(ctx, transactionHash, blockNumber)
	if err != nil {
		return false, err
	}
	if strings.ToLower(address) != strings.ToLower(eventTransfer.From.Hex()) {
		log.Errorf("front-end from address: %v\n event from address: %v\n",
			strings.ToLower(address),
			strings.ToLower(eventTransfer.From.Hex()),
		)
		return false, fmt.Errorf("from address mismatch")
	}
	if amount.Cmp(eventTransfer.Tokens) != 0 {
		return false, fmt.Errorf("amount mismatch, on-chain: %v, got: %v", eventTransfer.Tokens.String(), amount.String())
	}
	return true, nil
}

func (c *arpaClient) StartInfuraHeartbeat(
	infuraHeartbeatInterval time.Duration,
	infuraHeartbeatRetryInterval time.Duration,
	infuraHeartbeatRetryTimes int,
) {
	go func() {
		for {
			time.Sleep(infuraHeartbeatInterval)
			// fail and retry to ensure the heartbeat is sent
			for i := 0; i < infuraHeartbeatRetryTimes; i++ {
				log.Infof("Eth: sending a heartbeat to Infura...")
				_, err := c.ethClient.HeaderByNumber(context.Background(), nil)
				if err != nil {
					log.Errorf("Eth: warning: failed to send a heartbeat to Infura")
					time.Sleep(infuraHeartbeatRetryInterval)
					continue
				}
				log.Infof("Eth: heartbeat to Infura sent")
				break
			}
		}
	}()
}

func (c *arpaClient) BackfillConfirmTransaction(
	stakeholderAddress string,
	amount string,
	transactionHash string,
) error {
	amountBig, ok := new(big.Int).SetString(amount, 10)
	if !ok {
		return fmt.Errorf("big.Int SetString failed")
	}
	transferConfirmed, err := c.ConfirmTransfer(
		context.Background(),
		stakeholderAddress,
		amountBig,
		transactionHash,
	)
	if err != nil {
		return fmt.Errorf("confirming transaction for tx hash: %v failed for reason: %v", transactionHash, err.Error())
	}
	if !transferConfirmed {
		return fmt.Errorf("confirming transaction for tx hash: %v failed", transactionHash)
	}
	return nil
}
