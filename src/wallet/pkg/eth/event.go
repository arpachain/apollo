package eth

import (
	"math/big"

	"github.com/ethereum/go-ethereum/common"
)

type EventTransfer struct {
	From   common.Address
	To     common.Address
	Tokens *big.Int
}
