package main

import (
	"flag"
	"strings"
	"time"

	"github.com/arpachain/apollo/src/common/pkg/config"
	"github.com/arpachain/apollo/src/rest/pkg/router"
	"github.com/arpachain/apollo/src/rest/pkg/setup"
	"github.com/arpachain/apollo/src/staking/pkg/fission"
	"github.com/arpachain/apollo/src/wallet/pkg/eth"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/labstack/echo/v4"
	"github.com/labstack/gommon/log"
	"github.com/neo4j/neo4j-go-driver/neo4j"
	"golang.org/x/crypto/acme/autocert"
)

var (
	isDev                          = flag.Bool("is_dev", true, "indicate whether to use dev config")
	useHTTPS                       = flag.Bool("use_https", false, "toggle between http and https")
	hostname                       = flag.String("hostname", "dev-stake.arpachain.co", "The hostname for generating Let's Encrypt TLS certificate")
	address                        = flag.String("address", "127.0.0.1:1223", "The ip address and port number of the staking api server")
	ethClientURL                   = flag.String("eth_url", "wss://ropsten.infura.io/ws/v3/d89e8976ca54463faef78b643edfdae9", "the url for eth connection")
	dbServerIP                     = flag.String("db_ip", "localhost", "the ip of database server, dev environment will ignore this option")
	passphrase                     = flag.String("passphrase", "default_passphrase", "the passphrase used to encrypt keys")
	admissionPrivateKeyHex         = flag.String("admission_private_key", "0E3AF342AD3349DBAF3AEFEA16FEB5368E8AA124B2F13E0563D9B9D8900C9310", "the private key of admission fee account")
	arpaTokenAddress               = flag.String("token_address", "0xd1e00ce58a12e77e736a6417fb5fe6e0f08697ab", "the erc-20 contract address")
	numConfirmations               = flag.Int("num_confirmations", 3, "number of block confirmation for transaction")
	pollingIntervalForMining       = flag.Duration("interval_mining", 5*time.Second, "polling interval before tx is mined")
	pollingIntervalForConfirmation = flag.Duration("interval_confirm", 10*time.Second, "polling interval before tx is confirmed")
	waitingTime                    = flag.Duration("waiting_time", 30*time.Minute, "total waiting time for tx confirmation")
	allowedOriginsString           = flag.String("allowed_origins", "http://localhost:3000,https://localhost:3000", "give all allowed origins in a comma-separated string")
	infuraHeartbeatInterval        = flag.Duration("infura_heartbeat_interval", 15*time.Minute, "the interval between each heartbeat to prevent infura from dropping the connection")
	infuraHeartbeatRetryInterval   = flag.Duration("infura_retry_interval", time.Minute, "the interval between each retry when infura heartbeat fails")
	infuraHeartbeatRetryTimes      = flag.Int("infura_retry_times", 3, "the number of retries when infura heartbeat fails")
)

func main() {
	flag.Parse()
	allowedOrigins := strings.Split(*allowedOriginsString, ",")
	driver, err := neo4j.NewDriver("bolt://localhost:7687", neo4j.BasicAuth("neo4j", "apollo", ""))
	if err != nil {
		log.Fatalf("neo4j connection failed: %v", err)
	}
	ethClient, err := ethclient.Dial(*ethClientURL)
	if err != nil {
		log.Fatalf("ethereum client connection failed: %v", err)
	}
	arpaClient := eth.NewARPAClient(
		ethClient,
		*arpaTokenAddress,
		int64(*numConfirmations),
		*pollingIntervalForMining,
		*pollingIntervalForConfirmation,
		*waitingTime,
	)
	admissionPrivateKey, err := eth.ParsePrivateKeyHex(*admissionPrivateKeyHex)
	if err != nil {
		log.Fatalf("parsing admission private key failed: %v", err)
	}
	admissionPublicKey, err := eth.DerivePublicKey(admissionPrivateKey)
	if err != nil {
		log.Fatalf("deriving admission public key failed: %v", err)
	}
	admissionAddress := eth.DeriveAddress(admissionPublicKey)
	defaultConfig := config.NewDefaultConfig(*isDev, admissionAddress)
	database := fission.NewDatabase(driver)
	manager := fission.NewManager(database, arpaClient, defaultConfig, "test")
	e := echo.New()
	e = setup.SetupMiddlewares(e, log.INFO, allowedOrigins)
	e = router.SetupFissionHandlerRoutes(e, manager, defaultConfig)
	if *useHTTPS {
		e.AutoTLSManager.HostPolicy = autocert.HostWhitelist(*hostname)
		e.AutoTLSManager.Cache = autocert.DirCache("/var/www/.cache")
		e.Logger.Fatal(e.StartAutoTLS(*address))
	} else {
		e.Logger.Fatal(e.Start(*address))
	}
}
