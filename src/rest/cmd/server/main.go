package main

import (
	"flag"
	"strings"
	"time"

	"github.com/arpachain/apollo/src/rest/pkg/setup"
	"golang.org/x/crypto/acme/autocert"
)

var (
	isProd                         = flag.Bool("is_prod", false, "indicate whether to use dev config")
	useHTTPS                       = flag.Bool("use_https", false, "toggle between http and https")
	hostname                       = flag.String("hostname", "dev-stake.arpachain.co", "The hostname for generating Let's Encrypt TLS certificate")
	address                        = flag.String("address", "127.0.0.1:1223", "The ip address and port number of the staking api server")
	ethClientURL                   = flag.String("eth_url", "wss://ropsten.infura.io/ws/v3/d89e8976ca54463faef78b643edfdae9", "the url for eth connection")
	dbServerIP                     = flag.String("db_ip", "localhost", "the ip of database server, dev environment will ignore this option")
	passphrase                     = flag.String("passphrase", "default_passphrase", "the passphrase used to encrypt keys")
	admissionPrivateKeyHex         = flag.String("admission_private_key", "0E3AF342AD3349DBAF3AEFEA16FEB5368E8AA124B2F13E0563D9B9D8900C9310", "the private key of admission fee account")
	arpaTokenAddress               = flag.String("token_address", "0xd1e00ce58a12e77e736a6417fb5fe6e0f08697ab", "the erc-20 contract address")
	numConfirmations               = flag.Int("num_confirmations", 8, "number of block confirmation for transaction")
	pollingIntervalForMining       = flag.Duration("interval_mining", 10*time.Second, "polling interval before tx is mined")
	pollingIntervalForConfirmation = flag.Duration("interval_confirm", 30*time.Second, "polling interval before tx is confirmed")
	waitingTime                    = flag.Duration("waiting_time", 60*time.Minute, "total waiting time for tx confirmation")
	allowedOriginsString           = flag.String("allowed_origins", "http://localhost:3000,https://localhost:3000", "give all allowed origins in a comma-separated string")
	infuraHeartbeatInterval        = flag.Duration("infura_heartbeat_interval", 15*time.Minute, "the interval between each heartbeat to prevent infura from dropping the connection")
	infuraHeartbeatRetryInterval   = flag.Duration("infura_retry_interval", time.Minute, "the interval between each retry when infura heartbeat fails")
	infuraHeartbeatRetryTimes      = flag.Int("infura_retry_times", 3, "the number of retries when infura heartbeat fails")
	neo4jConnectionString          = flag.String("neo4j_connection_string", "bolt://localhost:7687", "connection string of neo4j database")
	neo4jUsername                  = flag.String("neo4j_username", "neo4j", "username for neo4j")
	neo4jPassword                  = flag.String("neo4j_password", "neo4j", "password for neo4j")
)

func main() {
	flag.Parse()
	allowedOrigins := strings.Split(*allowedOriginsString, ",")
	e := setup.SetupServer(
		*isProd,
		*ethClientURL,
		*dbServerIP,
		*passphrase,
		*admissionPrivateKeyHex,
		*arpaTokenAddress,
		int64(*numConfirmations),
		*pollingIntervalForMining,
		*pollingIntervalForConfirmation,
		*waitingTime,
		allowedOrigins,
		*infuraHeartbeatInterval,
		*infuraHeartbeatRetryInterval,
		*infuraHeartbeatRetryTimes,
		*neo4jConnectionString,
		*neo4jUsername,
		*neo4jPassword,
	)
	if *useHTTPS {
		e.AutoTLSManager.HostPolicy = autocert.HostWhitelist(*hostname)
		e.AutoTLSManager.Cache = autocert.DirCache("/var/www/.cache")
		e.Logger.Fatal(e.StartAutoTLS(*address))
	} else {
		e.Logger.Fatal(e.Start(*address))
	}
}
