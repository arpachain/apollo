package node

import (
	"net/http"

	"github.com/arpachain/apollo/src/common/pkg/config"
	"github.com/arpachain/apollo/src/staking/pkg/stake"
	"github.com/arpachain/apollo/src/staking/pkg/stakeholder"
	"github.com/ethereum/go-ethereum/common"
	"github.com/globalsign/mgo/bson"
	"github.com/labstack/echo/v4"
)

type Handler struct {
	StakeholderManager stakeholder.Manager
	StakeManager       stake.Manager
	DefaultConfig      config.DefaultConfig
}

func (h *Handler) ReadAllNodes(c echo.Context) (err error) {
	nodes, err := h.StakeholderManager.GetAllNodes()
	if err != nil {
		return &echo.HTTPError{Code: http.StatusInternalServerError, Message: "Internal server error"}
	}
	nodeResponses := make([]*NodeResponse, 0)
	for _, node := range nodes {
		if node.Activated && node.Votable {
			stakingAmount, err := h.StakeManager.GetTotalStakingAmount(node.ID)
			if err != nil {
				return &echo.HTTPError{Code: http.StatusInternalServerError, Message: "Internal server error"}
			}
			nodeResponse := &NodeResponse{
				NodeID:        node.ID.Hex(),
				NodeName:      node.Name,
				NodeType:      node.Type,
				Address:       node.Address,
				StakingAmount: stakingAmount.String(),
				Activated:     node.Activated,
				Votable:       node.Votable,
				ReturnRate:    h.DefaultConfig.GetReturnRate(node.Type, stakingAmount) * 0.9,
			}
			nodeResponses = append(nodeResponses, nodeResponse)
		}
	}
	return c.JSON(http.StatusOK, nodeResponses)
}

func (h *Handler) CreateNode(c echo.Context) (err error) {
	req := &CreateNodeRequest{}
	if err = c.Bind(req); err != nil {
		return &echo.HTTPError{Code: http.StatusInternalServerError, Message: "Internal server error"}
	}
	if !common.IsHexAddress(req.Address) {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: "Invalid stake holder address"}
	}
	nodeAddress, nodeID, err := h.StakeholderManager.SetupNewNode(req.Name, req.Address, req.NodeType)
	if err != nil {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: err.Error()}
	}
	nodeConfig := h.DefaultConfig.GetNodeConfigByType(req.NodeType)
	res := &CreateNodeResponse{
		NodeAddress:      nodeAddress,
		AdmissionAddress: h.DefaultConfig.GetAdmissionAddress(),
		AdmissionFee:     nodeConfig.AdmissionFee,
		MinStakingAmount: nodeConfig.MinimumStakingAmount,
		NodeID:           nodeID.Hex(),
	}
	return c.JSON(http.StatusCreated, res)
}

func (h *Handler) ReadNodes(c echo.Context) (err error) {
	req := &ReadNodesRequest{}
	if err = c.Bind(req); err != nil {
		return &echo.HTTPError{Code: http.StatusInternalServerError, Message: "Internal server error"}
	}
	if !common.IsHexAddress(req.Address) {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: "Invalid stake holder address"}
	}
	nodes, err := h.StakeholderManager.GetNodes(req.Address)
	if err != nil {
		return &echo.HTTPError{Code: http.StatusInternalServerError, Message: "Internal server error"}
	}
	nodeResponses := make([]*NodeResponse, 0)
	for _, node := range nodes {
		stakingAmount, err := h.StakeManager.GetStakingAmount(node.ID, req.Address)
		if err != nil {
			return &echo.HTTPError{Code: http.StatusInternalServerError, Message: "Internal server error"}
		}
		totalStakingAmount, err := h.StakeManager.GetTotalStakingAmount(node.ID)
		if err != nil {
			return &echo.HTTPError{Code: http.StatusInternalServerError, Message: "Internal server error"}
		}
		nodeConfig := h.DefaultConfig.GetNodeConfigByType(node.Type)
		nodeResponse := &NodeResponse{
			NodeID:               node.ID.Hex(),
			NodeName:             node.Name,
			NodeType:             node.Type,
			Address:              node.Address,
			AdmissionAddress:     h.DefaultConfig.GetAdmissionAddress(),
			AdmissionFee:         nodeConfig.AdmissionFee,
			StakingAmount:        stakingAmount.String(),
			TotalStakingAmount:   totalStakingAmount.String(),
			MinimumStakingAmount: nodeConfig.MinimumStakingAmount,
			Activated:            node.Activated,
			Votable:              node.Votable,
			ReturnRate:           h.DefaultConfig.GetReturnRate(node.Type, stakingAmount),
		}
		nodeResponses = append(nodeResponses, nodeResponse)
	}
	return c.JSON(http.StatusOK, nodeResponses)
}

func (h *Handler) ReadNode(c echo.Context) (err error) {
	req := &ReadNodeRequest{}
	if err = c.Bind(req); err != nil {
		return &echo.HTTPError{Code: http.StatusInternalServerError, Message: "Internal server error"}
	}
	if !bson.IsObjectIdHex(req.NodeID) {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: "Invalid node id"}
	}
	node, err := h.StakeholderManager.GetNode(bson.ObjectIdHex(req.NodeID))
	if err != nil {
		return &echo.HTTPError{Code: http.StatusInternalServerError, Message: "Internal server error"}
	}
	nodeResponses := make([]*NodeResponse, 0)
	stakingAmount, err := h.StakeManager.GetStakingAmount(node.ID, node.StakeholderAddress)
	if err != nil {
		return &echo.HTTPError{Code: http.StatusInternalServerError, Message: "Internal server error"}
	}
	totalStakingAmount, err := h.StakeManager.GetTotalStakingAmount(node.ID)
	if err != nil {
		return &echo.HTTPError{Code: http.StatusInternalServerError, Message: "Internal server error"}
	}
	nodeConfig := h.DefaultConfig.GetNodeConfigByType(node.Type)
	nodeResponse := &NodeResponse{
		NodeID:               node.ID.Hex(),
		NodeName:             node.Name,
		NodeType:             node.Type,
		Address:              node.Address,
		AdmissionAddress:     h.DefaultConfig.GetAdmissionAddress(),
		AdmissionFee:         nodeConfig.AdmissionFee,
		StakingAmount:        stakingAmount.String(),
		TotalStakingAmount:   totalStakingAmount.String(),
		MinimumStakingAmount: nodeConfig.MinimumStakingAmount,
		Activated:            node.Activated,
		Votable:              node.Votable,
		ReturnRate:           h.DefaultConfig.GetReturnRate(node.Type, stakingAmount),
	}
	nodeResponses = append(nodeResponses, nodeResponse)
	return c.JSON(http.StatusOK, nodeResponses)
}

func (h *Handler) ReadStakingStatus(c echo.Context) (err error) {
	req := &StakingStatusRequest{}
	if err = c.Bind(req); err != nil {
		return &echo.HTTPError{Code: http.StatusInternalServerError, Message: "Internal server error"}
	}
	if !common.IsHexAddress(req.Address) {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: "Invalid stake holder address"}
	}
	excludedNodes, err := h.StakeholderManager.GetNodes(req.Address)
	if err != nil {
		return &echo.HTTPError{Code: http.StatusInternalServerError, Message: "Internal server error"}
	}
	excludedNodeIDs := make([]string, 0)
	for _, excludedNode := range excludedNodes {
		excludedNodeIDs = append(excludedNodeIDs, excludedNode.ID.Hex())
	}
	nodeIDs, err := h.StakeManager.GetStakingOnlyNodeIDs(req.Address, excludedNodeIDs)
	if err != nil {
		return &echo.HTTPError{Code: http.StatusInternalServerError, Message: "Internal server error"}
	}
	responses := make([]*StakingStatusResponse, 0)
	for _, nodeID := range nodeIDs {
		node, err := h.StakeholderManager.GetNode(bson.ObjectIdHex(nodeID))
		if err != nil {
			return &echo.HTTPError{Code: http.StatusInternalServerError, Message: "Internal server error"}
		}
		stakingAmount, err := h.StakeManager.GetStakingAmount(node.ID, req.Address)
		if err != nil {
			return err
		}
		totalStakingAmount, err := h.StakeManager.GetTotalStakingAmount(node.ID)
		if err != nil {
			return &echo.HTTPError{Code: http.StatusInternalServerError, Message: "Internal server error"}
		}
		response := &StakingStatusResponse{
			NodeName:           node.Name,
			NodeType:           node.Type,
			NodeID:             node.ID.Hex(),
			NodeAddress:        node.Address,
			StakingAmount:      stakingAmount.String(),
			TotalStakingAmount: totalStakingAmount.String(),
			ReturnRate:         h.DefaultConfig.GetReturnRate(node.Type, totalStakingAmount) * 0.9,
		}
		responses = append(responses, response)
	}
	return c.JSON(http.StatusOK, responses)
}
