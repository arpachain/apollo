package node

import (
	"github.com/arpachain/apollo/src/common/pkg/config"
)

type CreateNodeRequest struct {
	Name     string          `json:"name"`
	Address  string          `json:"address"`
	NodeType config.NodeType `json:"nodeType"`
}

type ReadNodesRequest struct {
	Address string `json:"address"`
}

type ReadNodeRequest struct {
	NodeID string `json:"nodeId"`
}

type StakingStatusRequest struct {
	Address string `json:"address"`
}
