package node

import (
	"github.com/arpachain/apollo/src/common/pkg/config"
)

type CreateNodeResponse struct {
	NodeAddress      string `json:"nodeAddress"`
	AdmissionAddress string `json:"admissionAddress"`
	AdmissionFee     string `json:"admissionFee"`
	MinStakingAmount string `json:"minStakingAmount"`
	NodeID           string `json:"nodeId"`
}

type NodeResponse struct {
	NodeName             string          `json:"name"`
	NodeType             config.NodeType `json:"nodeType"`
	NodeID               string          `json:"nodeId"`
	Address              string          `json:"address"`
	StakingAmount        string          `json:"stakingAmount"`
	TotalStakingAmount   string          `json:"totalStakingAmount"`
	AdmissionFee         string          `json:"admissionFee"`
	AdmissionAddress     string          `json:"admissionAddress"`
	MinimumStakingAmount string          `json:"minimumStakingAmount"`
	Activated            bool            `json:"activated"`
	Votable              bool            `json:"votable"`
	ReturnRate           float32         `json:"returnRate"`
}

type StakingStatusResponse struct {
	NodeName           string          `json:"name"`
	NodeType           config.NodeType `json:"nodeType"`
	NodeID             string          `json:"nodeId"`
	NodeAddress        string          `json:"nodeAddress"`
	StakingAmount      string          `json:"stakingAmount"`
	TotalStakingAmount string          `json:"totalStakingAmount"`
	ReturnRate         float32         `json:"returnRate"`
}
