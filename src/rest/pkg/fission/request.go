package fission

type CreateNodeRequest struct {
	Name          string `json:"name"`
	Address       string `json:"address"`
	ParentAddress string `json:"parentAddress"`
}

type ReadNodeRequest struct {
	HolderAddress string `json:"holderAddress"`
}

type ReadChildNodeRequest struct {
	HolderAddress string `json:"holderAddress"`
	Level         int    `json:"level"`
}

type StakeRequest struct {
	TransactionHash    string `json:"transactionHash"`
	TotalAmount        string `json:"totalAmount"`
	StakeholderAddress string `json:"stakeholderAddress"`
}

type ReadRankingsRequest struct {
	Limit int `json:"limit"`
}
