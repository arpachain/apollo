package fission

type CreateNodeResponse struct {
	NodeAddress      string `json:"nodeAddress"`
	MinStakingAmount string `json:"minStakingAmount"`
}

type ReadNodeResponse struct {
	HolderAddress    string  `json:"holderAddress"`
	Name             string  `json:"name"`
	Address          string  `json:"address"`
	Activated        bool    `json:"activated"`
	StakingAmount    string  `json:"stakingAmount"`
	MinStakingAmount string  `json:"minStakingAmount"`
	TransactionHash  string  `json:"transactionHash"`
	ReturnRate       float32 `json:"returnRate"`
	LevelOne         int64   `json:"levelOne"`
	LevelTwo         int64   `json:"levelTwo"`
	LevelThree       int64   `json:"levelThree"`
	TotalAward       int64   `json:"totalAward"`
}
