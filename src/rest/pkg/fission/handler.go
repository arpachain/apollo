package fission

import (
	"math/big"
	"net/http"

	"github.com/labstack/gommon/log"

	"github.com/arpachain/apollo/src/common/pkg/config"
	"github.com/arpachain/apollo/src/staking/pkg/fission"
	"github.com/ethereum/go-ethereum/common"
	"github.com/labstack/echo/v4"
)

type Handler struct {
	FissionManager fission.Manager
	DefaultConfig  config.DefaultConfig
}

func (h *Handler) CreateNode(c echo.Context) (err error) {
	req := &CreateNodeRequest{}
	if err = c.Bind(req); err != nil {
		return &echo.HTTPError{Code: http.StatusInternalServerError, Message: "Internal server error"}
	}
	if !common.IsHexAddress(req.Address) {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: "Invalid holder address"}
	}
	if req.ParentAddress != "" && !common.IsHexAddress(req.ParentAddress) {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: "Invalid parent holder address"}
	}
	nodeAddress, err := h.FissionManager.SetupNewNode(req.Name, req.Address, req.ParentAddress)
	if err != nil {
		log.Errorf("creating fission node failed for reason: %v", err)
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: err.Error()}
	}
	res := &CreateNodeResponse{
		NodeAddress:      nodeAddress,
		MinStakingAmount: h.DefaultConfig.GetFissionNodeMinStakingAmtStr(),
	}
	return c.JSON(http.StatusCreated, res)
}

func (h *Handler) ReadNode(c echo.Context) (err error) {
	req := &ReadNodeRequest{}
	if err = c.Bind(req); err != nil {
		return &echo.HTTPError{Code: http.StatusInternalServerError, Message: "Internal server error"}
	}
	if !common.IsHexAddress(req.HolderAddress) {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: "Invalid holder address"}
	}
	node, err := h.FissionManager.GetNode(req.HolderAddress)
	if err != nil {
		return c.JSON(http.StatusOK, err.Error())
	}
	if node == nil {
		return &echo.HTTPError{Code: http.StatusInternalServerError, Message: "Internal server error"}
	}
	row, err := h.FissionManager.GetNodeInfo(req.HolderAddress)
	if err != nil {
		return c.JSON(http.StatusOK, err.Error())
	}
	if row == nil {
		return &echo.HTTPError{Code: http.StatusInternalServerError, Message: "Internal server error"}
	}
	response := h.mapNodeToResponse(node, row)
	response.ReturnRate = h.DefaultConfig.GetFissionNodeReturnRate()
	return c.JSON(http.StatusOK, response)
}

// func (h *Handler) ReadChildNodes(c echo.Context) (err error) {
// 	req := &ReadChildNodeRequest{}
// 	if err = c.Bind(req); err != nil {
// 		return &echo.HTTPError{Code: http.StatusInternalServerError, Message: "Internal server error"}
// 	}
// 	if !common.IsHexAddress(req.HolderAddress) {
// 		return &echo.HTTPError{Code: http.StatusBadRequest, Message: "Invalid holder address"}
// 	}
// 	nodes, err := h.FissionManager.GetChildNodes(req.HolderAddress, req.Level)
// 	nodeResponses := make([]*ReadNodeResponse, 0)
// 	for _, node := range nodes {
// 		nodeResponses = append(nodeResponses, h.mapNodeToResponse(node))
// 	}
// 	return c.JSON(http.StatusOK, nodeResponses)
// }

func (h *Handler) Stake(c echo.Context) (err error) {
	req := &StakeRequest{}
	if err = c.Bind(req); err != nil {
		return &echo.HTTPError{Code: http.StatusInternalServerError, Message: "Internal server error"}
	}
	if !common.IsHexAddress(req.StakeholderAddress) {
		return &echo.HTTPError{Code: http.StatusBadRequest, Message: "Invalid stake holder address"}
	}
	node, err := h.FissionManager.GetNode(req.StakeholderAddress)
	if node == nil {
		return &echo.HTTPError{Code: http.StatusNotFound, Message: "node-non-exist"}
	}
	if err != nil {
		log.Errorf("get node failed: %v", err)
		return &echo.HTTPError{Code: http.StatusInternalServerError, Message: "Internal server error"}
	}
	if node.Activated {
		log.Errorf("node already activated with tx hash: %v", node.TransactionHash)
		return &echo.HTTPError{Code: http.StatusInternalServerError, Message: "node-already-staked"}
	}
	totalAmount := new(big.Int)
	totalAmount, ok := totalAmount.SetString(req.TotalAmount, 10)
	if !ok {
		log.Errorf("big.Int SetString failed")
		return &echo.HTTPError{Code: http.StatusInternalServerError, Message: "Internal server error"}
	}
	err = h.FissionManager.ActivateNode(
		req.StakeholderAddress,
		totalAmount,
		req.TransactionHash,
	)
	if err != nil {
		log.Errorf("node initial staking failed: %v", err)
		return &echo.HTTPError{Code: http.StatusInternalServerError, Message: "stake-failed"}
	}
	return c.JSON(http.StatusAccepted, true)
}

func (h *Handler) mapNodeToResponse(node *fission.Node, row *fission.Row) *ReadNodeResponse {
	return &ReadNodeResponse{
		HolderAddress:    node.HolderAddress,
		Name:             node.Name,
		Address:          node.Address,
		Activated:        node.Activated,
		StakingAmount:    node.StakingAmount,
		MinStakingAmount: h.DefaultConfig.GetFissionNodeMinStakingAmtStr(),
		TransactionHash:  node.TransactionHash,
		LevelOne:         row.LevelOne,
		LevelTwo:         row.LevelTwo,
		LevelThree:       row.LevelThree,
		TotalAward:       row.TotalAward,
	}
}

func (h *Handler) ReadRankings(c echo.Context) (err error) {
	req := &ReadRankingsRequest{}
	if err = c.Bind(req); err != nil {
		return &echo.HTTPError{Code: http.StatusInternalServerError, Message: "Internal server error"}
	}
	rows, err := h.FissionManager.GetRankings(req.Limit)
	if err != nil {
		return &echo.HTTPError{Code: http.StatusInternalServerError, Message: err.Error()}
	}
	return c.JSON(http.StatusOK, rows)
}

func (h *Handler) BackfillNodes() {
	unconfirmedNodes, err := h.FissionManager.GetUnconfirmedNodes()
	if err != nil {
		log.Errorf("backfill fission nodes failed for reason: %v", err.Error())
		return
	}
	log.Infof("backfilling %v fission nodes...", len(unconfirmedNodes))
	for _, unconfirmedNode := range unconfirmedNodes {
		go func(node *fission.Node) {
			err := h.FissionManager.BackfillConfirmTransaction(
				node.HolderAddress,
				node.StakingAmount,
				node.TransactionHash,
			)
			if err != nil {
				log.Errorf("confirming stake failed for reason: %v", err.Error())
				return
			}
			err = h.FissionManager.BackfillNode(node.HolderAddress)
			if err != nil {
				log.Errorf("confirming stake failed for reason: %v", err.Error())
				return
			}
		}(unconfirmedNode)
	}
}
