package router

import (
	handler "github.com/arpachain/apollo/src/rest/pkg/stake"
	"github.com/arpachain/apollo/src/staking/pkg/stake"
	"github.com/arpachain/apollo/src/staking/pkg/stakeholder"
	"github.com/labstack/echo/v4"
)

func SetupStakeHandlerRoutes(
	e *echo.Echo,
	stakeholderManager stakeholder.Manager,
	stakeManager stake.Manager,
) *echo.Echo {
	h := &handler.Handler{
		StakeHolderManager: stakeholderManager,
		StakeManager:       stakeManager,
	}
	h.BackfillAdmissions()
	h.BackfillStakes()
	e.POST("v1/stake/apply", h.Apply)
	e.POST("v1/stake/stake", h.Stake)
	e.POST("v1/stake/unstake", h.Unstake)
	return e
}
