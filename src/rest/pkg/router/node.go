package router

import (
	"github.com/arpachain/apollo/src/common/pkg/config"
	"github.com/arpachain/apollo/src/rest/pkg/node"
	"github.com/arpachain/apollo/src/staking/pkg/stake"
	"github.com/arpachain/apollo/src/staking/pkg/stakeholder"
	"github.com/labstack/echo/v4"
)

func SetupNodeHandlerRoutes(
	e *echo.Echo,
	stakeholderManager stakeholder.Manager,
	stakeManager stake.Manager,
	defaultConfig config.DefaultConfig,
) *echo.Echo {
	h := &node.Handler{
		StakeholderManager: stakeholderManager,
		StakeManager:       stakeManager,
		DefaultConfig:      defaultConfig,
	}
	e.POST("v1/node/create", h.CreateNode)
	e.POST("v1/node/read_nodes", h.ReadNodes)
	e.POST("v1/node/read_node", h.ReadNode)
	e.POST("v1/node/read_staking_status", h.ReadStakingStatus)
	e.GET("v1/node/read_all", h.ReadAllNodes)
	return e
}
