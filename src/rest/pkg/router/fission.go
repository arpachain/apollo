package router

import (
	"github.com/arpachain/apollo/src/common/pkg/config"
	fh "github.com/arpachain/apollo/src/rest/pkg/fission"
	"github.com/arpachain/apollo/src/staking/pkg/fission"
	"github.com/labstack/echo/v4"
)

func SetupFissionHandlerRoutes(
	e *echo.Echo,
	fissionManager fission.Manager,
	defaultConfig config.DefaultConfig,
) *echo.Echo {
	h := fh.Handler{
		FissionManager: fissionManager,
		DefaultConfig:  defaultConfig,
	}
	h.BackfillNodes()
	e.POST("v1/fission/create", h.CreateNode)
	e.POST("v1/fission/stake", h.Stake)
	e.POST("v1/fission/read_node", h.ReadNode)
	// e.POST("v1/fission/read_child_nodes", h.ReadChildNodes)
	e.POST("v1/fission/read_rankings", h.ReadRankings)
	return e
}
