package setup

import (
	"github.com/labstack/gommon/log"

	"github.com/globalsign/mgo"
	"github.com/neo4j/neo4j-go-driver/neo4j"
)

func CreateMongoDBSession(serverIP string) *mgo.Session {
	db, err := mgo.Dial(serverIP)
	if err != nil {
		log.Fatal(err)
	}
	// if err = db.Copy().DB("apollo").C("nodes").EnsureIndex(mgo.Index{
	// 	Key:    []string{"stakeholderAddress"},
	// 	Unique: true,
	// }); err != nil {
	// 	log.Fatal(err)
	// }
	if err = db.Copy().DB("apollo").C("whitelist").EnsureIndex(mgo.Index{
		Key:    []string{"stakeholderAddress"},
		Unique: true,
	}); err != nil {
		log.Fatal(err)
	}
	if err = db.Copy().DB("apollo").C("stakes").EnsureIndex(mgo.Index{
		Key:    []string{"transactionHash"},
		Unique: true,
	}); err != nil {
		log.Fatal(err)
	}
	if err = db.Copy().DB("apollo").C("admissions").EnsureIndex(mgo.Index{
		Key:    []string{"nodeId"},
		Unique: true,
	}); err != nil {
		log.Fatal(err)
	}
	if err = db.Copy().DB("apollo").C("dividends").EnsureIndex(mgo.Index{
		Key:    []string{"stakeTransactionHash", "date"},
		Unique: true,
	}); err != nil {
		log.Fatal(err)
	}
	return db
}

func CreateNeo4jDBSession(connectionString, username, password string) neo4j.Driver {
	driver, err := neo4j.NewDriver(connectionString, neo4j.BasicAuth(username, password, ""))
	if err != nil {
		log.Fatalf("neo4j connection failed: %v", err)
	}
	return driver
}
