package setup

import (
	"net/http"

	"github.com/labstack/echo/v4"
	"github.com/labstack/echo/v4/middleware"
	"github.com/labstack/gommon/log"
)

func SetupMiddlewares(e *echo.Echo, logLevel log.Lvl, allowedOrigins []string) *echo.Echo {
	e.Use(
		middleware.Logger(),
		middleware.Recover(),
	)
	e.Logger.SetLevel(logLevel)
	for _, allowedOrigin := range allowedOrigins {
		log.Infof("allowing origin: %v", allowedOrigin)
	}
	e.Use(middleware.CORSWithConfig(middleware.CORSConfig{
		AllowOrigins: allowedOrigins,
		AllowMethods: []string{http.MethodGet, http.MethodPut, http.MethodPost, http.MethodDelete},
	}))
	return e
}
