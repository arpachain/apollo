package setup

import (
	"time"

	"github.com/arpachain/apollo/src/common/pkg/config"
	"github.com/arpachain/apollo/src/rest/pkg/router"
	"github.com/arpachain/apollo/src/staking/pkg/fission"
	"github.com/arpachain/apollo/src/staking/pkg/stake"
	"github.com/arpachain/apollo/src/staking/pkg/stakeholder"
	"github.com/arpachain/apollo/src/wallet/pkg/eth"
	"github.com/ethereum/go-ethereum/ethclient"
	"github.com/labstack/echo/v4"
	"github.com/labstack/gommon/log"
)

func SetupServer(
	isProd bool,
	ethClientURL string,
	dbServerIP string,
	passphrase string,
	admissionPrivateKeyHex string,
	arpaTokenAddress string,
	numConfirmations int64,
	pollingIntervalForMining time.Duration,
	pollingIntervalForConfirmation time.Duration,
	waitingTime time.Duration,
	allowedOrigins []string,
	infuraHeartbeatInterval time.Duration,
	infuraHeartbeatRetryInterval time.Duration,
	infuraHeartbeatRetryTimes int,
	neo4jConnectionString string,
	neo4jUsername string,
	neo4jPassword string,
) *echo.Echo {
	ethClient, err := ethclient.Dial(ethClientURL)
	if err != nil {
		log.Fatalf("ethereum client connection failed: %v", err)
	}
	arpaClient := eth.NewARPAClient(
		ethClient,
		arpaTokenAddress,
		numConfirmations,
		pollingIntervalForMining,
		pollingIntervalForConfirmation,
		waitingTime,
	)
	arpaClient.StartInfuraHeartbeat(
		infuraHeartbeatInterval,
		infuraHeartbeatRetryInterval,
		infuraHeartbeatRetryTimes,
	)
	var stakeholderDB stakeholder.Database
	var stakeDB stake.Database
	var logLevel log.Lvl
	admissionPrivateKey, err := eth.ParsePrivateKeyHex(admissionPrivateKeyHex)
	if err != nil {
		log.Fatalf("parsing admission private key failed: %v", err)
	}
	admissionPublicKey, err := eth.DerivePublicKey(admissionPrivateKey)
	if err != nil {
		log.Fatalf("deriving admission public key failed: %v", err)
	}
	admissionAddress := eth.DeriveAddress(admissionPublicKey)
	defaultConfig := config.NewDefaultConfig(isProd, admissionAddress)
	db := CreateMongoDBSession(dbServerIP)
	stakeholderDB = stakeholder.NewMongo(db)
	stakeDB = stake.NewMongo(db)
	stakeholderManager := stakeholder.NewManager(passphrase, stakeholderDB)
	stakeManager := stake.NewManager(stakeDB, arpaClient, defaultConfig)
	e := echo.New()
	if isProd {
		log.Infof("production environment")
		logLevel = log.ERROR
		e.Debug = false
		e.HideBanner = true
	} else {
		log.Infof("development environment")
		logLevel = log.DEBUG
		e.Debug = true
		e.HideBanner = false
	}
	log.SetLevel(logLevel)
	e = SetupMiddlewares(e, logLevel, allowedOrigins)
	e = router.SetupNodeHandlerRoutes(e, stakeholderManager, stakeManager, defaultConfig)
	e = router.SetupStakeHandlerRoutes(e, stakeholderManager, stakeManager)
	driver := CreateNeo4jDBSession(neo4jConnectionString, neo4jUsername, neo4jPassword)
	neo4jDatabase := fission.NewDatabase(driver)
	manager := fission.NewManager(neo4jDatabase, arpaClient, defaultConfig, passphrase)
	e = router.SetupFissionHandlerRoutes(e, manager, defaultConfig)

	return e
}
