package stake

import (
	"math/big"
	"net/http"

	"github.com/labstack/gommon/log"

	"github.com/arpachain/apollo/src/staking/pkg/stake"
	"github.com/arpachain/apollo/src/staking/pkg/stakeholder"
	"github.com/globalsign/mgo/bson"
	"github.com/labstack/echo/v4"
)

type Handler struct {
	StakeManager       stake.Manager
	StakeHolderManager stakeholder.Manager
}

func (h *Handler) Stake(c echo.Context) (err error) {
	req := &StakeRequest{}
	if err = c.Bind(req); err != nil {
		return &echo.HTTPError{Code: http.StatusInternalServerError, Message: "Internal server error"}
	}
	if !bson.IsObjectIdHex(req.NodeID) {
		log.Infof("invalid id: %v", req.NodeID)
		return &echo.HTTPError{Code: http.StatusInternalServerError, Message: "Internal server error"}
	}
	node, err := h.StakeHolderManager.GetNode(bson.ObjectIdHex(req.NodeID))
	if node == nil {
		return &echo.HTTPError{Code: http.StatusNotFound, Message: "node-non-exist"}
	}
	if err != nil {
		log.Infof("get node failed: %v", err)
		return &echo.HTTPError{Code: http.StatusInternalServerError, Message: "Internal server error"}
	}
	if !node.Activated {
		return &echo.HTTPError{Code: http.StatusInternalServerError, Message: "node-not-activated"}
	}
	totalAmount := new(big.Int)
	totalAmount, ok := totalAmount.SetString(req.TotalAmount, 10)
	if !ok {
		log.Infof("big.Int SetString failed")
		return &echo.HTTPError{Code: http.StatusInternalServerError, Message: "Internal server error"}
	}
	err = h.StakeManager.Stake(
		totalAmount,
		req.StakeholderAddress,
		req.TransactionHash,
		!node.Votable, // when the node is not votable, it means this should be the initial stake
		node.Type,
		node.ID,
	)
	if err != nil {
		log.Infof("staking failed: %v", err)
		return &echo.HTTPError{Code: http.StatusInternalServerError, Message: "stake-failed"}
	}
	if !node.Votable {
		err = h.StakeHolderManager.AllowVoteForNode(node.ID)
		if err != nil {
			log.Infof("allowing to vote failed: %v", err)
			return &echo.HTTPError{Code: http.StatusInternalServerError, Message: "Internal server error"}
		}
	}
	return c.JSON(http.StatusAccepted, true)
}

func (h *Handler) Apply(c echo.Context) (err error) {
	req := &ApplyRequest{}
	if err = c.Bind(req); err != nil {
		return &echo.HTTPError{Code: http.StatusInternalServerError, Message: "Internal server error"}
	}
	if !bson.IsObjectIdHex(req.NodeID) {
		log.Infof("invalid id: %v", req.NodeID)
		return &echo.HTTPError{Code: http.StatusInternalServerError, Message: "Internal server error"}
	}
	node, err := h.StakeHolderManager.GetNode(bson.ObjectIdHex(req.NodeID))
	if node == nil {
		return &echo.HTTPError{Code: http.StatusNotFound, Message: "node-non-exist"}
	}
	if err != nil {
		log.Infof("get node failed: %v", err)
		return &echo.HTTPError{Code: http.StatusInternalServerError, Message: "Internal server error"}
	}
	if node.Activated {
		log.Infof("node already activated with address: %v", node.Address)
		return &echo.HTTPError{Code: http.StatusInternalServerError, Message: "node-already-activated"}
	}
	admissionFeeAmount := new(big.Int)
	admissionFeeAmount, ok := admissionFeeAmount.SetString(req.AdmissionFeeAmount, 10)
	if !ok {
		log.Infof("big.Int SetString failed: %v", req.AdmissionFeeAmount)
		return &echo.HTTPError{Code: http.StatusInternalServerError, Message: "Internal server error"}
	}
	err = h.StakeManager.Apply(
		admissionFeeAmount,
		node.StakeholderAddress,
		req.TransactionHash,
		node.Type,
		node.ID,
	)
	if err != nil {
		log.Infof("node admission failed: %v", err)
		return &echo.HTTPError{Code: http.StatusInternalServerError, Message: "apply-failed"}
	}
	err = h.StakeHolderManager.ActivateNode(node.ID)
	if err != nil {
		log.Infof("node activation failed: %v", err)
		return &echo.HTTPError{Code: http.StatusInternalServerError, Message: "Internal server error"}
	}
	return c.JSON(http.StatusAccepted, true)
}

func (h *Handler) Unstake(c echo.Context) (err error) {
	return c.JSON(http.StatusLocked, "Stakes are currently locked")
}

func (h *Handler) BackfillAdmissions() {
	unconfirmedAdmissions, err := h.StakeManager.GetUnconfirmedAdmissions()
	if err != nil {
		log.Errorf("backfill admissions failed for reason: %v", err.Error())
		return
	}
	log.Infof("backfilling %v admissions...", len(unconfirmedAdmissions))
	for _, unconfirmedAdmission := range unconfirmedAdmissions {
		go func(admission *stake.Admission) {
			err := h.StakeManager.BackfillConfirmTransaction(
				admission.StakeholderAddress,
				admission.Amount,
				admission.TransactionHash,
			)
			if err != nil {
				log.Errorf("confirming admission failed for reason: %v", err.Error())
				return
			}
			err = h.StakeManager.ConfirmAdmission(admission.NodeID)
			if err != nil {
				log.Errorf("confirming admission failed for reason: %v", err.Error())
				return
			}
			err = h.StakeHolderManager.ActivateNode(admission.NodeID)
			if err != nil {
				log.Errorf("activating node failed for reason: %v", err.Error())
				return
			}
			log.Infof("backfilling node: %v ...", admission.NodeID.Hex())
		}(unconfirmedAdmission)
	}
	log.Infof("admission backfill kicked off...")
}

func (h *Handler) BackfillStakes() {
	unconfirmedStakes, err := h.StakeManager.GetUnconfirmedStakes()
	if err != nil {
		log.Errorf("backfill stakes failed for reason: %v", err.Error())
		return
	}
	log.Infof("backfilling %v stakes...", len(unconfirmedStakes))
	for _, unconfirmedStake := range unconfirmedStakes {
		go func(stake *stake.Stake) {
			err := h.StakeManager.BackfillConfirmTransaction(
				stake.StakeholderAddress,
				stake.Amount,
				stake.TransactionHash,
			)
			if err != nil {
				log.Errorf("confirming stake failed for reason: %v", err.Error())
				return
			}
			err = h.StakeManager.ConfirmStake(stake.TransactionHash)
			if err != nil {
				log.Errorf("confirming stake failed for reason: %v", err.Error())
				return
			}
			if stake.IsInitialStake {
				h.StakeHolderManager.AllowVoteForNode(stake.NodeID)
			}
		}(unconfirmedStake)
	}
	log.Infof("stake backfill kicked off...")
}
