package stake

type ApplyRequest struct {
	TransactionHash    string `json:"transactionHash"`
	AdmissionFeeAmount string `json:"admissionFeeAmount"`
	StakeholderAddress string `json:"stakeholderAddress"`
	NodeID             string `json:"nodeId"`
}

type StakeRequest struct {
	TransactionHash    string `json:"transactionHash"`
	TotalAmount        string `json:"totalAmount"`
	StakeholderAddress string `json:"stakeholderAddress"`
	NodeID             string `json:"nodeId"`
}
