MATCH (n) DETACH DELETE n;
DROP CONSTRAINT ON (node:FissionNode) ASSERT node.HolderAddress IS UNIQUE;
CREATE CONSTRAINT ON (node:FissionNode) ASSERT node.HolderAddress IS UNIQUE;
CREATE (root:FissionNode{Activated: true, HolderAddress: "root", Name: "root_node"});